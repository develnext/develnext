<?php
namespace develnext\forms\ide\std\project;

use develnext\IDEForm;
use php\lang\Module;
use php\swing\UIWindow;

class NewProject extends IDEForm
{
    function __construct(UIWindow $window, Module $module = null, array $vars = array())
    {
        parent::__construct($window, $module, $vars);
    }
}