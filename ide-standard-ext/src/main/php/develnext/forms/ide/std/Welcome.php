<?php
namespace develnext\forms\ide\std;

use develnext\IDEForm;
use develnext\lang\DI;
use develnext\Manager;
use develnext\project\ProjectManager;
use develnext\project\UIProjectChooser;
use php\lang\Module;
use php\swing\event\MouseEvent;
use php\swing\Image;
use php\swing\UIButton;
use php\swing\UIDialog;
use php\swing\UIElement;
use php\swing\UIListbox;
use php\swing\UIPanel;
use php\swing\UIWindow;

/**
 * Class IDEWelcome
 *
 * @property UIButton $btnCreateProject
 * @property UIButton $btnOpenProject
 * @property UIButton $btnImportProject
 * @property UIElement $listLatestProjects
 * @property UIPanel $itemsPanel
 */
class IDEWelcome extends IDEForm {

    function __construct(UIWindow $window, Module $module = null, array $vars = array())
    {
        parent::__construct($window, $module, $vars);

        $this->listLatestProjects->on('click', function(MouseEvent $e) {
            /** @var UIListbox $list */
            $list = $e->target;

            if ($e->clickCount > 1 && $list->selectedIndex > -1) {
                /** @var ProjectManager $manager */
                $manager = DI::get(ProjectManager::class);
                $project = $manager->getLatestProject($list->selectedIndex);

                $manager->closeProject();
                $project = $manager->openProject($project->getDirectory());
                if (!$project) {
                    UIDialog::message(_('Cannot open project'), _('Error'), UIDialog::ERROR_MESSAGE);
                } else {
                    $this->hide(true);
                }
            }
        });
    }

    public function addItem($name, $description, Image $icon = null) {
        $item = new UIButton();
        $item->align = 'top';
        $item->h     = 50;
        $item->horAlignment = 2;
        $item->cursor = 'hand';
        $item->text  = "<html><b>{$name}</b><br><small>{$description}</small></html>";
        $item->setIcon($icon);

        $this->itemsPanel->add($item);

        $gap = new UIPanel();
        $gap->align = 'top';
        $gap->h = 10;

        $this->itemsPanel->add($gap);

        $this->itemsPanel->h += $item->h + $gap->h;

        return $item;
    }
}