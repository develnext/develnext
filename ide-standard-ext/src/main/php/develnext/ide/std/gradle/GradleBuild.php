<?php
namespace develnext\ide\std\gradle;

use develnext\ide\std\project\dependency\group\GUIRuntimeDependency;
use develnext\ide\std\project\dependency\group\JPHPRuntimeDependency;
use develnext\ide\std\project\dependency\MavenProjectDependency;
use develnext\project\Project;
use php\io\File;
use php\io\FileStream;
use php\io\Stream;
use php\lib\items;
use php\lib\str;
use php\util\Flow;

GradleBuild::setDependencyWriter(MavenProjectDependency::class, function (Stream $out, MavenProjectDependency $dep) {
    $out->write("   compile '{$dep->toString()}'\n");
});

GradleBuild::setDependencyWriter(JPHPRuntimeDependency::class, function (Stream $out, JPHPRuntimeDependency $dep) {
    $out->write("   compile '{$dep->getGroupId()}:jphp-runtime:{$dep->getVersion()}'\n");
});

GradleBuild::setDependencyWriter(GUIRuntimeDependency::class, function (Stream $out, GUIRuntimeDependency $dep) {
    $out->write("   compile '{$dep->getGroupId()}:jphp-swing-ext:{$dep->getVersion()}'\n");
});

class GradleBuild
{
    /** @var Project */
    protected $project;

    protected $group = '';
    protected $version = '1.0';

    /** @var array */
    protected $wrapper = [];

    /** @var array */
    protected $repositories = [];

    /** @var array */
    protected $dependencies = [];

    /** @var array */
    protected $buildscriptRepositories = [];

    /** @var array */
    protected $buildscriptDependencies = [];

    /** @var GradleBuild */
    protected $subProjects = [];

    /** @var array */
    protected $plugins = [];

    /** @var callable[] */
    protected $beforeHandlers = [];

    /** @var callable[] */
    protected $afterHandlers = [];

    /** @var callable[] */
    protected $beforeBuildscriptHandlers = [];

    /** @var callable[] */
    protected $afterBuildscriptHandlers = [];

    /** @var callable[] */
    protected static $dependencyWriters = [];

    public function __construct(Project $project)
    {
        $this->project = $project;
    }

    public function setWrapper($version)
    {
        if ($version) {
            $this->wrapper = [
                'version' => $version,
                'url' => "http://services.gradle.org/distributions/gradle-$version-bin.zip"
            ];
        } else {
            $this->wrapper = [];
        }
    }

    public static function setDependencyWriter($class, callable $writer) {
        self::$dependencyWriters[str::lower($class)] = $writer;
    }

    public function addAfterHandler(callable $callback, $prepend = false) {
        if ($prepend) {
            $this->afterHandlers = items::toList($callback, $this->afterHandlers);
        } else {
            $this->afterHandlers[] = $callback;
        }
    }

    public function addBeforeHandler(callable $callback, $prepend = false) {
        if ($prepend) {
            $this->beforeHandlers = items::toList($callback, $this->beforeHandlers);
        } else {
            $this->beforeHandlers[] = $callback;
        }
    }

    public function addBeforeBuildscriptHandler(callable $callback, $prepend = false) {
        if ($prepend) {
            $this->beforeBuildscriptHandlers = items::toList($callback, $this->beforeBuildscriptHandlers);
        } else {
            $this->beforeBuildscriptHandlers[] = $callback;
        }
    }

    public function addAfterBuildscriptHandler(callable $callback, $prepend = false) {
        if ($prepend) {
            $this->afterBuildscriptHandlers = items::toList($callback, $this->afterBuildscriptHandlers);
        } else {
            $this->afterBuildscriptHandlers[] = $callback;
        }
    }

    public function addRepository($repository)
    {
        $this->repositories[$repository] = $repository;
    }

    public function addDependency($dependency)
    {
        $this->dependencies[] = $dependency;
    }

    public function addBuildscriptRepository($repository)
    {
        $this->buildscriptRepositories[$repository] = $repository;
    }

    public function addBuildscriptDependency($dependency)
    {
        $this->buildscriptDependencies[$dependency] = $dependency;
    }

    public function addPlugin($plugin, $prepend = false)
    {
        if ($prepend) {
            $this->plugins = items::toList($plugin, $this->plugins);
        } else {
            $this->plugins[$plugin] = $plugin;
        }
    }

    public function addSubProject($name, GradleBuild $build)
    {
        $this->subProjects[$name] = $build;
    }

    public function writeTo($directory)
    {
        $stream = new FileStream($directory . '/build.gradle', 'w+');

        try {
            foreach ($this->beforeHandlers as $handler) {
                $handler($stream);
            }

            if ($this->buildscriptDependencies || $this->buildscriptRepositories || $this->beforeBuildscriptHandlers
                    || $this->afterBuildscriptHandlers) {

                $stream->write("buildscript {\n");

                foreach ($this->beforeBuildscriptHandlers as $handler) {
                    $handler($stream);
                }

                if ($this->buildscriptRepositories) {
                    $stream->write("\trepositories {\n");

                    $stream->write("\t\tmavenLocal()\n");
                    $stream->write("\t\tmavenCentral()\n");
                    $stream->write("\t\tjcenter()\n");

                    $stream->write("\n");

                    foreach ($this->buildscriptRepositories as $repository) {
                        $stream->write("\t\tmaven { url '$repository'}\n");
                    }

                    $stream->write("\t}\n");
                }

                if ($this->buildscriptDependencies) {
                    $stream->write("\tdependencies {\n");

                    foreach ($this->buildscriptDependencies as $dependency) {
                        $stream->write("\t\tclasspath '$dependency'\n");
                    }

                    $stream->write("\t}\n");
                }

                foreach ($this->afterBuildscriptHandlers as $handler) {
                    $handler($stream);
                }

                $stream->write("}\n\n");
            }

            foreach ($this->plugins as $plugin) {
                $stream->write("apply plugin: '$plugin'\n");
            }

            $stream->write("\n");

            $stream->write("group   = '{$this->project->getName()}'\n");
            $stream->write("version = '1.0'\n");

            $stream->write("\n");

            $stream->write("repositories {\n");

            $stream->write("    mavenLocal()\n");
            $stream->write("    mavenCentral()\n");
            $stream->write("    jcenter()\n");

            if ($this->repositories) {
                $stream->write("\n");
            }

            foreach ($this->repositories as $repository) {
                $stream->write("    maven { url '$repository' }\n");
            }

            $stream->write("}\n\n");

            $stream->write("dependencies {\n");

            foreach (Flow::of($this->project->getDependencies())->append($this->dependencies) as $dep) {
                $writer = self::$dependencyWriters[str::lower(get_class($dep))];

                if ($writer) {
                    $writer($stream, $dep);
                } else if (is_string($dep)) {
                    $stream->write("    compile '$dep'\n");
                }
            }

            $stream->write("}\n");

            $this->_writeWrapper($stream);

            foreach ($this->afterHandlers as $handler) {
                $handler($stream);
            }
        } finally {
            $stream->close();
        }
    }

    protected function _writeWrapper(Stream $out)
    {
        if ($this->wrapper) {
            $out->write("\n");

            $out->write("task wrapper(type: Wrapper) {\n");
            $out->write("\tgradleVersion   = '{$this->wrapper["version"]}'\n");
            $out->write("\tdistributionUrl = '{$this->wrapper["url"]}'\n");

            $out->write("}\n");
        }
    }
}