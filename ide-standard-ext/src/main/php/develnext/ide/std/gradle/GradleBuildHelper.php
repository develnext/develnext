<?php
namespace develnext\ide\std\gradle;

final class GradleBuildHelper {

    public static function registerDevelNextArtifacts(GradleBuild $build) {
        $repository         = null;
        $snapshotRepository = 'https://oss.sonatype.org/content/repositories/snapshots';

        $build->registerArtifact('develnext.org:jphp-core', $repository, $snapshotRepository);
        $build->registerArtifact('develnext.org:jphp-runtime', $repository, $snapshotRepository);
        $build->registerArtifact('develnext.org:jphp-android', $repository, $snapshotRepository);
        $build->registerArtifact('develnext.org:jphp-gradle-plugin', $repository, $snapshotRepository);
        $build->registerArtifact('develnext.org:jphp-swing-ext', $repository, $snapshotRepository);
        $build->registerArtifact('develnext.org:jphp-http-ext', $repository, $snapshotRepository);
        $build->registerArtifact('develnext.org:jphp-json-ext', $repository, $snapshotRepository);
        $build->registerArtifact('develnext.org:jphp-zend-ext', $repository, $snapshotRepository);
    }
}