<?php

namespace develnext\ide\std\project\type;

use develnext\ide\std\project\dependency\group\GUIRuntimeDependency;
use develnext\ide\std\project\dependency\MavenProjectDependency;
use develnext\project\Project;
use php\io\FileStream;
use php\util\Flow;

class SwingProjectType extends PhpGradleProjectType
{
    function getName()
    {
        return 'Swing GUI';
    }

    function getDefaultDependencies()
    {
        return Flow::of(parent::getDefaultDependencies())->append([
            new MavenProjectDependency('org.develnext', 'jphp-swing-ext', '0.5-SNAPSHOT')
        ])->toArray();
    }

    function init(Project $project)
    {
        parent::init($project);

        $this->gradleBuild->addPlugin('java');
        $this->gradleBuild->addDependency(self::JPHP_MAVEN_GROUP_ID . ':jphp-swing-ext:' . self::JPHP_MAVEN_VERSION );
    }

    protected function getIcon()
    {
        return 'images/icons/projecttype/gui';
    }

    function onCreateProject(Project $project)
    {
        parent::onCreateProject($project);

        $project->addDependency(new GUIRuntimeDependency());

        $project->getFile('resources/forms')->mkdirs();
        $project->getFile('resources/images')->mkdirs();
    }
}
