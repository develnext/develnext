<?php
namespace develnext\ide\std\project\extension;

use develnext\ide\IdeManager;
use develnext\project\Project;
use develnext\project\ProjectExtension;

class AndroidProjectExtension extends ProjectExtension
{
    public function getUid()
    {
        return "android";
    }

    protected function onRegister()
    {
        $this->addExtension(GradleProjectExtension::class);
    }

}