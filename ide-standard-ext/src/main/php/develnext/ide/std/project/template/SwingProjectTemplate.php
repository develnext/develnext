<?php
namespace develnext\ide\std\project\template;

use develnext\project\Project;
use develnext\project\ProjectTemplate;
use php\swing\Image;

class SwingProjectTemplate extends ProjectTemplate {
    /**
     * @return string
     */
    public function getUid()
    {
        return "swing";
    }

    /**
     * @return string
     */
    public function getCategoryUid()
    {
        return "gui";
    }

    /**
     * @return string
     */
    public function getName()
    {
        return _('GUI Application');
    }

    public function getDescription()
    {
        return _('Classic GUI Application based on Swing');
    }


    /**
     * @param Project $project
     * @return mixed
     */
    public function updateProject(Project $project)
    {

    }
}