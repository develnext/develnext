<?php
namespace develnext\ide\std\project\extension;

use develnext\ide\IdeManager;
use develnext\ide\std\filetype\PhpFileType;
use develnext\project\Project;
use develnext\project\ProjectExtension;

class PhpProjectExtension extends ProjectExtension
{
    public function getUid()
    {
        return "php";
    }

    protected function onRegister()
    {

    }
}