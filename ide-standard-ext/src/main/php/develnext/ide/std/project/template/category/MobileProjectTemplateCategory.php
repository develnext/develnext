<?php
namespace develnext\ide\std\project\template\category;

use develnext\DN;
use develnext\ide\ImageManager;
use develnext\project\ProjectTemplateCategory;
use php\swing\Image;

class MobileProjectTemplateCategory extends ProjectTemplateCategory {
    /**
     * @return string
     */
    public function getUid()
    {
        return "mobile";
    }

    /**
     * @return Image
     */
    public function getIcon()
    {
        return DN::image('images/icons/projecttype/mobile32.png');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return _('Mobile');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return _('Mobile Application');
    }
}