<?php
namespace develnext\ide\std\project\template;

use develnext\project\Project;
use develnext\project\ProjectTemplate;

class JavaFXProjectTemplate extends ProjectTemplate
{

    /**
     * @return string
     */
    public function getUid()
    {
        return "javafx";
    }

    /**
     * @return string
     */
    public function getCategoryUid()
    {
        return "gui";
    }

    /**
     * @return string
     */
    public function getName()
    {
        return _('JavaFX Application');
    }

    public function getDescription()
    {
        return _('GUI Application based on CSS3 and HTML5');
    }


    /**
     * @param Project $project
     * @return mixed
     */
    public function updateProject(Project $project)
    {

    }
}