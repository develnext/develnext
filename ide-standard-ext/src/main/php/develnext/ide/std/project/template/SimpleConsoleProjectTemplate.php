<?php
namespace develnext\ide\std\project\template;


use develnext\DN;
use develnext\ide\std\project\extension\GradleProjectExtension;
use develnext\ide\std\project\extension\JPHPProjectExtension;
use develnext\ide\std\project\extension\ProgramProjectExtension;
use develnext\ide\wizard\ProjectCreationIdeWizard;
use develnext\lang\DI;
use develnext\project\Project;
use develnext\project\ProjectLoader;
use develnext\project\ProjectTemplate;
use php\swing\Image;

class SimpleConsoleProjectTemplate extends ProjectTemplate
{
    /**
     * @return string
     */
    public function getUid()
    {
        return "simple_console";
    }

    /**
     * @return string
     */
    public function getCategoryUid()
    {
        return "console";
    }

    public function getIcon()
    {
        return DN::image('images/icons/projecttype/console16.png');
    }

    public function getBigIcon()
    {
        return DN::image('images/icons/projecttype/console32.png');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return _('Simple Console Application');
    }

    /**
     * @param Project $project
     * @return mixed
     */
    public function updateProject(Project $project)
    {
        $this->addExtension($project, JPHPProjectExtension::class);
    }
}