<?php
namespace develnext\ide\std\project\extension;

use develnext\ide\IdeManager;
use develnext\ide\std\project\dependency\group\JPHPRuntimeDependency;
use develnext\project\Project;
use develnext\project\ProjectExtension;

class JPHPProjectExtension extends ProjectExtension
{
    public function getUid()
    {
        return "jphp";
    }

    public function _onRegister()
    {
        $this->addExtension(PhpProjectExtension::class);
        $this->addExtension(GradleProjectExtension::class);
        $this->addExtension(ProgramProjectExtension::class);
    }


}