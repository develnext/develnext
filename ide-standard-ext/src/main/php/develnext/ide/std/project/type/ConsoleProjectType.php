<?php
namespace develnext\ide\std\project\type;

use develnext\project\Project;

class ConsoleProjectType extends PhpGradleProjectType {

    function getName() {
        return 'Console';
    }

    protected function getIcon() {
        return 'images/icons/projecttype/console';
    }

    function init(Project $project)
    {
        parent::init($project);

        $this->gradleBuild->addPlugin('java');
    }
}
