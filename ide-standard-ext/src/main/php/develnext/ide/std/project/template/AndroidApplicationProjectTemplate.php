<?php
namespace develnext\ide\std\project\template;

use develnext\DN;
use develnext\ide\ImageManager;
use develnext\ide\wizard\ProjectCreationIdeWizard;
use develnext\project\Project;
use develnext\project\ProjectTemplate;
use php\swing\Image;

class AndroidApplicationProjectTemplate extends ProjectTemplate {
    /**
     * @return string
     */
    public function getUid()
    {
        return "android_application";
    }

    /**
     * @return string
     */
    public function getCategoryUid()
    {
        return "mobile";
    }

    /**
     * @return Image
     */
    public function getIcon()
    {
        return DN::image('images/icons/projecttype/android16.png');
    }

    /**
     * @return Image
     */
    public function getBigIcon()
    {
        return DN::image('images/icons/projecttype/android32.png');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return _('Android Application');
    }

    /**
     * @param Project $project
     * @return mixed
     */
    public function updateProject(Project $project)
    {

    }
}