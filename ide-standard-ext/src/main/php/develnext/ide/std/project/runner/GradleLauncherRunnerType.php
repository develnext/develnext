<?php
namespace develnext\ide\std\project\runner;

use develnext\DN;
use develnext\ide\components\UIMessages;
use develnext\ide\IdeManager;
use develnext\ide\ImageManager;
use develnext\ide\std\ide\ConsoleIdeTool;
use develnext\ide\std\project\type\GradleProjectType;
use develnext\jdi\VirtualMachine;
use develnext\Manager;
use develnext\project\Project;
use develnext\project\ProjectRunner;
use develnext\project\RunnerType;
use develnext\tool\GradleTool;
use php\lib\items;
use php\lib\str;
use php\swing\Color;
use php\swing\UICheckbox;
use php\swing\UICombobox;
use php\swing\UIContainer;
use php\swing\UIDialog;
use php\swing\UIEdit;
use php\swing\UILabel;
use php\swing\UIListbox;
use php\swing\UIPanel;
use php\util\Flow;

class GradleLauncherRunnerType extends RunnerType
{
    protected static $commands = ['run' => 'run', 'distZip' => 'distZip'];

    /**
     * @return string
     */
    public function getName()
    {
        return 'Gradle Launcher';
    }

    public function executeDebug(ProjectRunner $runner)
    {
        $this->_execute($runner, true);
    }

    public function execute(ProjectRunner $runner)
    {
        $this->_execute($runner, false);
    }

    protected function _execute(ProjectRunner $runner, $debug)
    {
        /** @var ConsoleIdeTool $console */
        if ($runner->isSingleton()) {
            $console = $debug ? $runner->debugConsole : $runner->console;
        }

        $runner->setDone(false);

        if (!$console) {
            $gradle = new GradleTool();
            $console = DN::ide()->openTool(
                'console',
                $runner->getTitle(),
                DN::image($runner->getType()->getIcon())
            );

            if ($runner->getConfig()['command'] === 'distZip') {
                $console->getUiTabHead()->setIcon(DN::image('images/icons/lorry_box16.png'));
            } else if ($debug) {
                $console->getUiTabHead()->setIcon(DN::image('images/icons/debug16.png'));
            } else {
                $console->getUiTabHead()->setIcon(DN::image('images/icons/run16.png'));
            }

            // buttons
            $console->addButton('run', DN::image('images/icons/play16.png'));
            $console->addSeparator();
            $console->addButton('stop', DN::image('images/icons/stop16.png'));
            $console->addButton('restart', DN::image('images/icons/arrow_refresh16.png'));

            $console->on('log', function (ConsoleIdeTool $self) {
                $self->setStatus('working');
            });

            /** @var VirtualMachine $vm */
            $vm = null;

            $console->on('process:after', function (ConsoleIdeTool $self, $text, $type) {
                if ($text == ':run') {
                    $style = $self->getConsole()->getStyle('std');
                    $style->foreground = Color::rgb(0,0,0);
                    $self->appendText("-------------- Stdout: -------------\n");
                }
            });

            $console->on('process:before', function (ConsoleIdeTool $self, $text, $type) use ($runner, $debug, &$vm) {
                if ($text == ':run') {
                    if ($debug) {
                        $vm = VirtualMachine::of('localhost:9009');
                        $vm->resume();
                    }
                }

                if ($text == 'BUILD SUCCESSFUL') {
                    $self->appendText("------------------------------------\n");
                    $style = $self->getConsole()->addStyle('std');
                    $style->foreground = Color::decode('#585858');

                    $self->setStatus('finished');
                    $runner->setDone(true);
                }
            });

            $onRun = function () use ($console, $gradle, $runner, $debug) {
                $args = items::toList(
                    $runner->getConfig()['command'],
                    str::split($runner->getConfig()['program_arguments'], ' ')
                );

                if ($debug) {
                    $args[] = '-Ddebug=1';
                    $args[] = '-DdebugPort=9009';
                }

                $console->logTool(
                    $gradle,
                    DN::project()->getDirectory(),
                    $args
                );
            };
            $console->on('btn-run', $onRun);

            $console->on('close', function (ConsoleIdeTool $self) use ($runner, $debug) {
                if ($debug) {
                    unset($runner->debugConsole);
                } else {
                    unset($runner->console);
                }

                return true;
            });

            $console->on('btn-stop', function () use ($console, $gradle, $vm) {
                $console->appendText('Stopping... ', 'err-b');

                if ($vm) {
                    $vm->resume();
                    $vm->halt(0);
                }
            });

            if ($debug) {
                $runner->debugConsole = $console;
            } else {
                $runner->console = $console;
            }
        }

        $console->trigger('btn-run');
    }

    /**
     * @param UIContainer $settingsPanel
     * @return mixed
     */
    public function createSettingsPanel(UIContainer $settingsPanel)
    {
        $label = new UILabel();
        $label->align = 'top';
        $label->h = 25;
        $label->text = __('{Command}:');

        $edit = new UICombobox();
        $edit->align = 'top';
        $edit->group = 'command';
        $edit->readOnly = false;
        $edit->setItems(self::$commands);
        $edit->h = 30;

        $settingsPanel->add($label);
        $settingsPanel->add($edit);

        $hr = new UIPanel();
        $hr->align = 'top';
        $hr->h = 10;
        $hr->background = [0, 0, 0, 0];
        $settingsPanel->add($hr);

        $label = new UILabel();
        $label->align = 'top';
        $label->h = 25;
        $label->text = __('{Program arguments}:');

        $edit = new UIEdit();
        $edit->align = 'top';
        $edit->group = 'program-arguments';
        $edit->h = 30;

        $settingsPanel->add($label);
        $settingsPanel->add($edit);

        $showBuildDialog = new UICheckbox();
        $showBuildDialog->align = 'top';
        $showBuildDialog->text = _('Show dialog after building');
        $showBuildDialog->h = 35;
        $showBuildDialog->group = 'show-dialog-after-building';
        $settingsPanel->add($showBuildDialog);
    }

    /**
     * @param array $config
     * @param UIContainer $settingsPanel
     * @return mixed
     */
    public function loadConfig(array $config, UIContainer $settingsPanel)
    {
        /** @var UIListbox $edit */
        $edit = $settingsPanel->getComponentByGroup('command');

        /** @var UIEdit $programArguments */
        $programArguments = $settingsPanel->getComponentByGroup('program-arguments');
        $programArguments->text = $config['program_arguments'];

        if (self::$commands[$config['command']]) {
            $i = 0;
            foreach (self::$commands as $key => $value) {
                if ($key === $config['command'])
                    break;
                $i++;
            }

            $edit->selectedIndex = $i;
        }

        /** @var UICheckbox $showDialog */
        $showDialog = $settingsPanel->getComponentByGroup('show-dialog-after-building');
        $showDialog->selected = $config['show_dialog_after_building'];
    }

    /**
     * @param UIContainer $settingsPanel
     * @return array
     */
    public function fetchConfig(UIContainer $settingsPanel)
    {
        /** @var UIListbox $edit */
        $edit = $settingsPanel->getComponentByGroup('command');

        /** @var UICheckbox $showDialog */
        $showDialog = $settingsPanel->getComponentByGroup('show-dialog-after-building');

        /** @var UIEdit $programArguments */
        $programArguments = $settingsPanel->getComponentByGroup('program-arguments');
        return [
            'command' => Flow::of(self::$commands)->skip($edit->selectedIndex)->current(),
            'show_dialog_after_building' => $showDialog->selected,
            'program_arguments' => $programArguments->text
        ];
    }

    public function isAvailable(Project $project)
    {
        return true;
    }
}
