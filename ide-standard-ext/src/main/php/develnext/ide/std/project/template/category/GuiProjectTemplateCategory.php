<?php
namespace develnext\ide\std\project\template\category;

use develnext\DN;
use develnext\ide\ImageManager;
use develnext\project\ProjectTemplateCategory;
use php\swing\Image;

class GuiProjectTemplateCategory extends ProjectTemplateCategory {
    /**
     * @return string
     */
    public function getUid()
    {
        return "gui";
    }

    /**
     * @return Image
     */
    public function getIcon()
    {
        return DN::image('images/icons/projecttype/gui32.png');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return _('GUI');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return _('Desktop UI Applications');
    }
}