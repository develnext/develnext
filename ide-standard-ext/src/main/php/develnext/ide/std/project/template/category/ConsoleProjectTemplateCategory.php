<?php
namespace develnext\ide\std\project\template\category;

use develnext\DN;
use develnext\ide\ImageManager;
use develnext\project\ProjectTemplateCategory;
use php\swing\Image;

class ConsoleProjectTemplateCategory extends ProjectTemplateCategory {
    /**
     * @return string
     */
    public function getUid()
    {
        return "console";
    }

    /**
     * @return Image
     */
    public function getIcon()
    {
        return DN::image('images/icons/projecttype/console32.png');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return _('Console');
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return _('Console Applications');
    }
}