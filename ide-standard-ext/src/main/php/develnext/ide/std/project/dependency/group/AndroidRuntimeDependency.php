<?php
namespace develnext\ide\std\project\dependency\group;


class AndroidRuntimeDependency extends JPHPRuntimeDependency
{
    function getUniqueCode()
    {
        return "android-runtime#";
    }

    function toString()
    {
        return "android-runtime";
    }
}