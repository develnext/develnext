<?php
namespace develnext\ide\std\project\dependency\group;


use develnext\project\ProjectDependency;

class GUIRuntimeDependency extends JPHPRuntimeDependency
{
    function getUniqueCode()
    {
        return "gui-runtime#";
    }

    function toString()
    {
        return "gui-runtime";
    }

    function fromString($string)
    {
        // nop
    }
}