<?php
namespace develnext\ide\std\project\dependency\group;

use develnext\project\ProjectDependency;

class JPHPRuntimeDependency extends ProjectDependency
{
    function getUniqueCode()
    {
        return "jphp-runtime#";
    }

    function toString()
    {
        return "jphp-runtime";
    }

    function fromString($string)
    {
        // nop`
    }

    function getGroupId()
    {
        return "org.develnext";
    }

    function getVersion()
    {
        return "0.5-SNAPSHOT";
    }
}