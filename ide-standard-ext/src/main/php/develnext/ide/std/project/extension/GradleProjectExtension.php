<?php
namespace develnext\ide\std\project\extension;

use develnext\ide\IdeManager;
use develnext\ide\std\wizard\RunConfigurationsWizard;
use develnext\project\Project;
use develnext\project\ProjectExtension;

class GradleProjectExtension extends ProjectExtension
{
    public function getUid()
    {
        return "gradle";
    }

    public function _onRegister()
    {
        $this->registerWizard(new RunConfigurationsWizard());
    }

    public function _onAfterLoad()
    {
        $this->_correct();
    }

    public function _onAfterCreate()
    {
        $this->_correct();
    }

    protected function _correct()
    {
        $this->project->getFile('src/')->mkdirs();
        $this->project->getFile('resources/')->mkdirs();
    }
}