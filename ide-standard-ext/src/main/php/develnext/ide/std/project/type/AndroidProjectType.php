<?php
namespace develnext\ide\std\project\type;

use develnext\ide\std\project\dependency\group\AndroidRuntimeDependency;
use develnext\project\Project;

class AndroidProjectType extends PhpGradleProjectType
{
    const GRADLE_VERSION_PLUGIN = '0.14.0';

    function getName()
    {
        return 'Android App';
    }

    protected function getIcon()
    {
        return 'images/icons/projecttype/android';
    }

    function init(Project $project)
    {
        parent::init($project);

        $this->gradleBuild->addBuildscriptDependency('com.android.tools.build:gradle:' . self::GRADLE_VERSION_PLUGIN);
        $this->gradleBuild->setWrapper('2.2');

        $this->gradleBuild->addPlugin('android', true);
    }

    function onCreateProject(Project $project)
    {
        parent::onCreateProject($project);

        $project->addDependency(new AndroidRuntimeDependency());
    }
}