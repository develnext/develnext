<?php
namespace develnext\ide\std\project\extension;

use develnext\ide\IdeManager;
use develnext\ide\std\wizard\RunConfigurationsWizard;
use develnext\project\Project;
use develnext\project\ProjectExtension;

class ProgramProjectExtension extends ProjectExtension
{
    public function getUid()
    {
        return "program";
    }

    protected function onRegister()
    {
        $this->registerWizard(new RunConfigurationsWizard());
    }
}