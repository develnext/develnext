<?php
namespace develnext\ide\std\project\type;

use develnext\ide\std\gradle\GradleBuild;
use develnext\ide\std\project\dependency\group\JPHPRuntimeDependency;
use develnext\ide\std\project\dependency\MavenProjectDependency;
use develnext\project\Project;
use php\io\Stream;

abstract class PhpGradleProjectType extends GradleProjectType
{
    const JPHP_MAVEN_GROUP_ID = 'org.develnext';
    const JPHP_MAVEN_VERSION  = '0.5-SNAPSHOT';

    function init(Project $project)
    {
        parent::init($project);

        $this->gradleBuild->addPlugin('java');
        $this->gradleBuild->addPlugin('application');
        $this->gradleBuild->addPlugin('php');

        $this->gradleBuild->addDependency(
            new MavenProjectDependency(self::JPHP_MAVEN_GROUP_ID, 'jphp-runtime', self::JPHP_MAVEN_VERSION)
        );

        $this->gradleBuild->addRepository('https://oss.sonatype.org/content/repositories/snapshots');

        $this->gradleBuild->addBuildscriptDependency(
            self::JPHP_MAVEN_GROUP_ID . ':jphp-gradle-plugin:' . self::JPHP_MAVEN_VERSION
        );

        $this->gradleBuild->addBuildscriptRepository('https://oss.sonatype.org/content/repositories/snapshots');

        $this->gradleBuild->addAfterHandler(function(Stream $out) {
            $out->write("\n");

            $out->write("sourceSets { main.resources.srcDirs = ['resources'] } \n\n");

            $out->write("mainClassName = 'php.runtime.launcher.StandaloneLauncher'\n\n");

            $out->write("if (System.getProperty('debug') in ['1', 'true']) {\n");
                $out->write("\tdef debugPort = System.getProperty('debugPort', '9009')\n");
                $out->write("\tapplicationDefaultJvmArgs = ['-Xdebug', '-Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=' + debugPort]\n");
            $out->write("}\n\n");

            $out->write("php {\n");
                $out->write("\tsrcDir = 'src'\n");
                $out->write("\textensions = ['php', 'jphp']\n");
                $out->write("\tcharset = 'UTF-8'\n");
            $out->write("}\n\n");

            $out->write("run.dependsOn(['buildPhp'])\n");
            $out->write("distZip.dependsOn(['buildPhp'])\n");
        });

        if (DEVELOPER_VERSION) {
            $handler = function(Stream $out) {
                $out->write("configurations.all {\n");
                $out->write("\tresolutionStrategy.cacheChangingModulesFor 15, 'seconds'\n");
                $out->write("}\n\n");
            };

            $this->gradleBuild->addBeforeHandler($handler);
            $this->gradleBuild->addBeforeBuildscriptHandler($handler);
        }
    }

    function onCreateProject(Project $project)
    {
        parent::onCreateProject($project);

        $project->addDependency(new JPHPRuntimeDependency());
    }


    function onCorrectProject(Project $project)
    {
        parent::onCorrectProject($project);

        $project->setFileMark($project->getProjectFile('libs/'), 'hidden');
        $project->setFileMark($project->getProjectFile('build.gradle'), 'hidden');
    }
}