<?php
namespace develnext\ide\std\dialog;

use develnext\DN;
use develnext\IDEForm;

class RunConfigurationDialog {
    /** @var IDEForm */
    protected $form;

    function __construct() {
        $this->form = DN::form('ide/std/project/RunConfigurations');
    }

    public function show() {
        $this->form->showModal();
    }
}
