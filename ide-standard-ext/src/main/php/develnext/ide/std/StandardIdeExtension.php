<?php
namespace develnext\ide\std;

use develnext\DN;
use develnext\filetype\creator\DirectoryFileTypeCreator;
use develnext\filetype\creator\FileFileTypeCreator;
use develnext\filetype\DirectoryFileType;
use develnext\filetype\ExternalRootDirectoryFileType;
use develnext\filetype\FileType;
use develnext\filetype\UnknownFileType;
use develnext\ide\IdeExtension;
use develnext\ide\IdeManager;
use develnext\ide\std\filetype\creator\PhpFileFileTypeCreator;
use develnext\ide\std\filetype\creator\SwingGuiFormFileTypeCreator;
use develnext\ide\std\filetype\GradleFileType;
use develnext\ide\std\filetype\ImageFileType;
use develnext\ide\std\filetype\PhpFileType;
use develnext\ide\std\filetype\SwingFormFileType;
use develnext\ide\std\filetype\TextFileType;
use develnext\ide\std\ide\ConsoleIdeTool;
use develnext\ide\std\project\runner\GradleLauncherRunnerType;
use develnext\ide\std\project\template\JavaFXProjectTemplate;
use develnext\ide\std\project\template\SwingProjectTemplate;
use develnext\ide\std\project\template\AndroidApplicationProjectTemplate;
use develnext\ide\std\project\template\category\ConsoleProjectTemplateCategory;
use develnext\ide\std\project\template\category\GuiProjectTemplateCategory;
use develnext\ide\std\project\template\category\MobileProjectTemplateCategory;
use develnext\ide\std\project\template\SimpleConsoleProjectTemplate;
use develnext\ide\std\project\type\AndroidProjectType;
use develnext\ide\std\project\type\ConsoleProjectType;
use develnext\ide\std\project\type\SwingProjectType;
use develnext\ide\std\wizard\AboutWizard;
use develnext\ide\std\wizard\DefaultWelcomeWizard;
use develnext\ide\std\wizard\IdeSettingsWizard;
use develnext\ide\std\wizard\NewProjectWizard;
use develnext\ide\std\wizard\OpenProjectWizard;
use develnext\ide\std\wizard\RunConfigurationsWizard;
use develnext\lang\DI;
use develnext\project\Project;
use develnext\tool\Tool;
use develnext\ui\decorator\UIListDecorator;
use php\io\File;
use php\swing\event\ItemEvent;
use php\swing\UICombobox;

class StandardIdeExtension extends IdeExtension {

    protected function registerMainMenu(IdeManager $manager) {
        $manager->addMenuGroup('file', _('File'));
            $manager->addMenuItem('file', 'open-project', _('Open Project'), 'images/icons/open16.png', 'control O');
            $manager->addMenuItem('file', 'close-project', _('Close Project'));
            $manager->addMenuSeparator('file');

            $manager->addMenuItem('file', 'save-all', _('Save All'), 'images/icons/save16.png', 'control S');
            $manager->addMenuItem('file', 'settings', _('Settings'), 'images/icons/settings16.png');
            $manager->addMenuSeparator('file');

            $manager->addMenuItem('file', 'exit', _('Exit'));

        // ------
        $manager->addMenuGroup('edit', _('Edit'));
            $manager->addMenuItem('edit', 'undo', _('Undo'), 'images/icons/undo16.png', 'control Z');
            $manager->addMenuItem('edit', 'redo', _('Redo'), 'images/icons/redo16.png', 'control shift Z');
            $manager->addMenuSeparator('edit');

            $manager->addMenuItem('edit', 'delete', _('Delete'), 'images/icons/cancel16.png', 'DELETE');

        // ------
        $manager->addMenuGroup('build', _('Build'));
            $manager->addMenuItem('build', 'run', _('Run'), 'images/icons/play16.png', 'F9');
            $manager->addMenuItem('build', 'debug', _('Debug'), 'images/icons/debug16.png', 'shift F9');

        // ------
        $manager->addMenuGroup('tools', _('Tools'));

        $manager->addMenuGroup('help', _('Help'));
    }

    protected function registerHeadMenu(IdeManager $manager) {
        $manager->addHeadMenuItem('file:open-project', 'images/icons/open16.png');
        $manager->addHeadMenuItem('file:save-all', 'images/icons/save16.png');
        $manager->addHeadMenuSeparator();

        $runCombo = new UICombobox();
        $runCombo->font = 'Tahoma 11';
        $runCombo->w = 120;
        $runCombo->group = 'run-list';
        $runCombo->readOnly = false;

        $runCombo->on('change', function(ItemEvent $e){
            if ($e->isSelected()) {
                /** @var UICombobox $target */
                $target = $e->target;

                $runner = Project::current()->getRunners()[$target->selectedIndex];
                Project::current()->selectRunner($runner);

                DN::ide()->findFromHeadMenu('build:run')->enabled = !!($runner);
            }
        });

        $manager->addHeadMenuAny($runCombo);
        $manager->addHeadMenuItem('build:run', 'images/icons/run16.png');
        $manager->addHeadMenuItem('build:debug', 'images/icons/debug16.png');
        $manager->addHeadMenuSeparator();

        $manager->addHeadMenuItem('file:settings', 'images/icons/settings16.png');
    }

    protected function registerPopupTreeMenu(IdeManager $manager) {
        $manager->addFileTreePopupGroup('new', _('Create'), 'images/icons/plus16.png');
        $manager->addFileTreePopupItem(null, 'edit:copy-files', __('{Add file/directory} ...'), 'images/icons/plus16.png');
            $manager->addFileTreePopupSeparator();
        $manager->addFileTreePopupItem(null, 'edit:delete', _('Delete'), 'images/icons/cancel16.png', 'DELETE');
        $manager->addFileTreePopupItem(null, '', _('Hide'));
            $manager->addFileTreePopupSeparator();
        $manager->addFileTreePopupItem(null, '', __('{Find in Path} ...'), 'images/icons/find16.png', 'control shift F');
        $manager->addFileTreePopupItem(null, '', __('{Replace in Path} ...'), null, 'control shift R');

        $manager->registerFileTypeCreator(new FileFileTypeCreator(), true);
        $manager->registerFileTypeCreator(new DirectoryFileTypeCreator(), true);
        $manager->addFileTreePopupSeparator('new');
        $manager->registerFileTypeCreator(new PhpFileFileTypeCreator(), true);
        $manager->registerFileTypeCreator(new SwingGuiFormFileTypeCreator(), true);
    }

    public function onRegister(IdeManager $manager) {
        $manager->addLocalizationPath('res://i18n/std');

        $this->registerMainMenu($manager);
        $this->registerHeadMenu($manager);
        $this->registerPopupTreeMenu($manager);


        $menuHandlers = new StandardIdeMenuHandlers();
        $manager->setMenuHandlers($menuHandlers->getHandlers());

        // tools
        $manager->registerIdeTool('console', new ConsoleIdeTool());

        // file types
        $manager->registerFileType(new UnknownFileType());
        $manager->registerFileType(new DirectoryFileType());

        $manager->registerFileType(new ExternalRootDirectoryFileType());
        $manager->registerFileType(new TextFileType());
        $manager->registerFileType(new PhpFileType());
        $manager->registerFileType(new SwingFormFileType());
        $manager->registerFileType(new GradleFileType());
        $manager->registerFileType(new ImageFileType());

        // project templates
        $manager->registerProjectTemplateCategory(new ConsoleProjectTemplateCategory());
        $manager->registerProjectTemplateCategory(new GuiProjectTemplateCategory());
        $manager->registerProjectTemplateCategory(new MobileProjectTemplateCategory());

        $manager->registerProjectTemplate(new SimpleConsoleProjectTemplate());
        $manager->registerProjectTemplate(new SwingProjectTemplate());
        $manager->registerProjectTemplate(new JavaFXProjectTemplate());
        $manager->registerProjectTemplate(new AndroidApplicationProjectTemplate());

        $manager->on('log-tool', function(IdeManager $manager,
                                          Tool $tool, File $directory, array $commands, callable $onEnd = null){
            /** @var ConsoleIdeTool $console */
            $console = $manager->openTool('console');
            $console->logTool($tool, $directory, $commands, $onEnd);
        });

        $manager->registerIdeWizard(new AboutWizard());
        $manager->registerIdeWizard(new DefaultWelcomeWizard());
        $manager->registerIdeWizard(new NewProjectWizard());
        $manager->registerIdeWizard(new OpenProjectWizard());
        $manager->registerIdeWizard(new RunConfigurationsWizard());
        $manager->registerIdeWizard(new IdeSettingsWizard());

        $manager->registerHrefProtocolHandler('wizard', function ($href, $params) {
            $wizard = DN::wizard($href);

            if (!$wizard) {
                throw new \Exception("Wizard '$href' is not found");
            }


            return $wizard->open($params['modal']);
        });
    }
}
