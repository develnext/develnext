<?php
namespace develnext\ide\std;

use develnext\DN;
use develnext\ide\components\UIDirectoryChooser;
use develnext\ide\std\dialog\RunConfigurationDialog;
use develnext\Manager;
use develnext\project\Project;
use develnext\project\ProjectFile;
use develnext\tool\GradleTool;
use develnext\tool\JavaTool;
use php\io\File;
use php\io\FileStream;
use php\lang\System;
use php\lib\str;
use php\swing\event\ComponentEvent;
use php\swing\event\MouseEvent;
use php\swing\event\SimpleEvent;
use php\swing\UIDialog;

/**
 * Class StandardIdeMenuHandlers
 * @package develnext\ide\std
 */
class StandardIdeMenuHandlers
{
    /** @var array */
    protected $handlers = [];

    public function __construct()
    {
        $elements = [
            'file:open-project', 'file:new-project', 'file:close-project', 'file:save-all', 'file:exit',
            'edit:undo', 'edit:redo', 'edit:delete', 'edit:copy-files',
            'build:run', 'build:debug', 'build:run-configurations'
        ];

        foreach ($elements as $el) {
            $handler = str::replace($el, ':', '_');
            $handler = str::replace($handler, '-', '');

            $this->handlers[$el] = function ($e) use ($handler) {
                $handle = [$this, $handler];
                if ($e->target->enabled) {
                    call_user_func($handle, $handle, $e);
                }
            };
        }
    }

    public function file_openProject()
    {

    }

    public function file_newProject()
    {

    }

    public function file_closeProject()
    {
        $bootstrap = DN::bootstrap();

        $manager = DN::projectManager();
        $manager->closeProject();

        DN::ide()->getMainForm()->hide();
        $bootstrap->showWelcome();
    }

    public function file_saveAll()
    {
        DN::project()->saveAll();
    }

    public function file_exit()
    {

    }

    public function edit_undo()
    {

    }

    public function edit_redo()
    {

    }

    public function edit_copyFiles()
    {
        $dialog = new UIDirectoryChooser('add_copy');
        $dialog->setOnlyDirectories(false);
        $dialog->showDialog();

        if ($files = $dialog->getSelectedFiles()) {
            $folder = DN::project()->getFileTree()->getCurrentFile();
            $folder = $folder->getFile();

            if ($folder->isFile()) {
                $folder = $folder->getParentFile();
            }

            $newFiles = [];
            foreach ($files as $file) {
                if ($file->isFile()) {
                    $fs = new FileStream($file);
                    $fs2 = new FileStream($newFiles[] = new File($folder, $file->getName()), 'w+');
                    $fs2->write($fs->readFully());

                    $fs->close();
                    $fs2->close();
                }
            }

            foreach ($newFiles as $file) {
                DN::project()->updateFile(
                    new ProjectFile($file, DN::project())
                );
            }
        }
    }

    public function edit_delete()
    {
        if (UIDialog::confirm(_('Are you sure?'), _('Question'), UIDialog::YES_NO_OPTION) == UIDialog::YES_OPTION) {
            $files = DN::project()->getFileTree()->getSelectedFiles();
            $deleted = [];
            foreach ($files as $file) {
                if ($file->delete()) {
                    $deleted[] = $file;
                }
            }

            foreach ($deleted as $file) {
                DN::project()->updateFile($file);
            }
        }
    }

    public function build_runConfigurations()
    {
        $dialog = new RunConfigurationDialog();
        $dialog->show();
    }

    public function build_debug($s, $e)
    {
        $this->build_run($s, $e, true);
    }

    public function build_run($s, $e, $debug = false)
    {
        $runner = Project::current()->getSelectedRunner();
        if (!$runner->isDone()) {
            $runner->stop();
        }

        $runner->execute($debug);
    }

    /**
     * @return array
     */
    public function getHandlers()
    {
        return $this->handlers;
    }
}
