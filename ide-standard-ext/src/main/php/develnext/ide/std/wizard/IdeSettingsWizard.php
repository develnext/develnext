<?php
namespace develnext\ide\std\wizard;

use develnext\ide\IdeManager;
use develnext\ide\IdeWizard;
use develnext\project\Project;

class IdeSettingsWizard extends IdeWizard
{
    public function create(Project $project = null)
    {
        $this->addMenuItem('file', 'ide-settings', _('IDE Settings'), null, 'control alt S');

        return parent::create($project);
    }

    public function available(Project $project = null)
    {
        return $project === null;
    }

    public function onOpen($modal = false)
    {
        // nop.
    }
}