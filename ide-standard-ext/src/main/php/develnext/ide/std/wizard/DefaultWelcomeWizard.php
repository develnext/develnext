<?php
namespace develnext\ide\std\wizard;

use develnext\DN;
use develnext\forms\ide\std\IDEWelcome;
use develnext\ide\IdeManager;
use develnext\ide\IdeWizard;
use develnext\ide\ImageManager;
use develnext\ide\wizard\WelcomeIdeWizard;
use develnext\lang\DI;
use develnext\Manager;
use develnext\project\Project;
use develnext\project\ProjectManager;
use develnext\ui\decorator\UIListDecorator;
use php\swing\Image;
use php\swing\UIListbox;

class DefaultWelcomeWizard extends WelcomeIdeWizard
{
    protected $items = [];

    public function available(Project $project = null)
    {
        return $project == null;
    }

    public function create(Project $project = null)
    {
        $this->addItem(
            _('Create Project'),
            _('Create a project based on template'),
            DN::image('images/icons/document_green32.png'),
            'wizard://' . NewProjectWizard::class . '?modal=1'
        );

        $this->addItem(
            _('Open Project'),
            _('Open a project created with DevelNext'),
            DN::image('images/icons/open_document32.png'),
            'wizard://' . OpenProjectWizard::class . '?modal=1'
        );

        return parent::create($project);
    }

    public function onHide($modalResult = null)
    {
        /** @var IDEWelcome $welcome */
        $welcome = DN::form('ide/std/Welcome');

        $welcome->hide($modalResult);
    }

    public function onOpen($modal = false)
    {
        /** @var IDEWelcome $welcome */
        $welcome = DN::form('ide/std/Welcome');

        $welcome->modalResult = false;

        $welcome->itemsPanel->removeAll();
        $welcome->itemsPanel->h = 0;

        foreach ($this->items as $item) {
            $button = $welcome->addItem($item['name'], $item['description'], $item['icon']);
            $button->on('click', function () use ($item, $welcome) {
                if (DN::open($item['href'])) {
                    $welcome->hide(true);
                }
            });
        }

        $list = $welcome->listLatestProjects;
        $listDecor = new UIListDecorator($list);
        $listDecor->clear();

        /** @var ProjectManager $projectManager */
        $projectManager = DI::get(ProjectManager::class);

        foreach ($projectManager->getLatestProjects() as $project) {
            $listDecor->add(
                $project->getName(),
                $project->getDirectory()->getPath(),
                $project->getTemplate() ? $project->getTemplate()->getBigIcon() : null
            );
        }

        if ($modal) {
            return $welcome->showModal();
        } else {
            return $welcome->show();
        }
    }

    public function addItem($name, $description, Image $icon = null, $href)
    {
        $this->items[] = [
            'name' => $name,
            'description' => $description,
            'icon' => $icon,
            'href' => $href
        ];
    }
}