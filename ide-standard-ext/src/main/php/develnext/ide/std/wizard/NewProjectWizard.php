<?php
namespace develnext\ide\std\wizard;

use develnext\DN;
use develnext\ide\IdeWizard;
use develnext\ide\wizard\WelcomeIdeWizard;
use develnext\IDEForm;
use develnext\project\Project;
use develnext\project\ProjectTemplate;
use develnext\project\ProjectTemplateCategory;
use develnext\ui\decorator\UIListDecorator;
use php\lib\items;
use php\swing\event\MouseEvent;
use php\swing\Scope;
use php\swing\UIButton;
use php\swing\UIListbox;

class NewProjectWizard extends IdeWizard
{
    /** @var ProjectTemplateCategory */
    protected $currentCategory;

    /** @var ProjectTemplate */
    protected $currentTemplate;

    public function available(Project $project = null)
    {
        return $project == null;
    }

    public function create(Project $project = null)
    {
        $this->addMenuItem('file', 'new-project', _('New Project'), 'images/icons/project16.png');
        $this->addHeadMenuItem('file:new-project', 'images/icons/project16.png');

        return parent::create($project);
    }

    public function onOpen($modal = false, array $arguments = [])
    {
        /** @var IDEForm $form */
        $form = DN::form('ide/std/project/NewProject');

        $this->_updateState($form);

        $templateList = new UIListDecorator($form['templateList']);
        $categoryList = new UIListDecorator($form['categoryList']);

        $categoryList->clear();

        /** @var ProjectTemplateCategory[] $templateCategories */
        $templateCategories = items::toArray(DN::ide()->getProjectTemplateCategories());

        foreach ($templateCategories as $category) {
            $categoryList->add($category->getName(), $category->getDescription(), $category->getIcon());
        }

        $categoryList->getElement()->on('click', function (MouseEvent $e) use ($templateCategories, $templateList, $form) {
            /** @var UIListbox $target */
            $target = $e->target;

            $templateList->clear();
            $this->currentTemplate = null;

            if ($this->currentCategory = $templateCategories[$target->selectedIndex]) {
                foreach ($this->currentCategory->getTemplates() as $template) {
                    $templateList->add(
                        $template->getName(),
                        $template->getDescription(),
                        $template->getBigIcon()
                    );
                }
            }

            if ($this->currentCategory) {
                $templateList->getElement()->selectedIndex = 0;
                $this->currentTemplate = $this->currentCategory->getTemplates()[0];
            }

            $this->_updateState($form);
        });

        $templateList->getElement()->on('click', function (MouseEvent $e) use (&$currentTemplate, $templateList, $form) {
            if ($this->currentCategory) {
                $templates = $this->currentCategory->getTemplates();

                $this->currentTemplate = $templates[$e->target->selectedIndex];
            }

            $this->_updateState($form);
        });

        if ($modal) {
            $form->showModal();
        } else {
            $form->show();
        }
    }

    protected function _updateState(IDEForm $form)
    {
        /** @var UIButton $btnCreate */
        $btnCreate = $form['btnCreate'];

        $btnCreate->enabled = !!$this->currentTemplate;

        $btnCreate->on('click', function (MouseEvent $e) use ($form) {
            if ($this->currentTemplate && $e->target->enabled) {
                $project = DN::projectManager()->createProject($this->currentTemplate);

                if ($project) {
                    $form->hide();
                    DN::wizard(WelcomeIdeWizard::class)->hide(true);
                }
            }
        });
    }
}