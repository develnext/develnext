<?php
namespace develnext\ide\std\wizard;

use develnext\DN;
use develnext\ide\IdeWizard;
use develnext\ide\std\dialog\RunConfigurationDialog;
use develnext\Manager;
use develnext\project\Project;

class RunConfigurationsWizard extends IdeWizard
{
    public function available(Project $project = null)
    {
        return false;
    }

    public function onCreate()
    {
        $this->addHeadMenuItem('build:run-configurations', 'images/icons/cog_add.png');
    }

    public function onOpen($modal = false)
    {
        $dialog = DN::form('ide/std/project/RunConfigurations');

        if ($modal) {
            $dialog->showModal();
        } else {
            $dialog->show();
        }
    }
}