<?php
namespace develnext\ide\std\wizard;

use develnext\DN;
use develnext\ide\IdeWizard;
use develnext\project\Project;

class AboutWizard extends IdeWizard
{
    public function onCreate()
    {
        $this->addMenuItem('help', 'about', _('About'), null, 'F1');
    }

    public function available(Project $project = null)
    {
        return $project == null;
    }

    public function onOpen($modal = false, array $arguments = [])
    {

    }
}