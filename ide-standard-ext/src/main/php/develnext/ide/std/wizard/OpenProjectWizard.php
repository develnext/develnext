<?php
namespace develnext\ide\std\wizard;

use develnext\DN;
use develnext\ide\IdeWizard;
use develnext\project\Project;
use develnext\project\UIProjectChooser;
use php\swing\UIDialog;

class OpenProjectWizard extends IdeWizard
{

    public function available(Project $project = null)
    {
        return $project === null;
    }

    public function create(Project $project = null)
    {
        $this->addMenuItem('file', 'open-project', _('Open Project'), 'images/icons/open16.png');
        $this->addHeadMenuItem('file:open-project', 'images/icons/open16.png');

        return parent::create($project);
    }

    public function onOpen($modal = false)
    {
        $dirDialog = new UIProjectChooser();
        $dirDialog->showDialog();

        if ($dirDialog->getSelectedFile()) {
            $manager = DN::projectManager();
            $manager->closeProject();
            $project = $manager->openProject($dirDialog->getSelectedFile());

            if (!$project) {
                UIDialog::message(_('Cannot open project'), _('Error'), UIDialog::ERROR_MESSAGE);
            } else {
                return true;
            }
        }
    }
}