<?php
namespace {

    use develnext\DN;
    use develnext\Bootstrap;
    use ext\javafx\layout\UXAnchorPane;
    use ext\javafx\UXApplication;
    use php\io\IOException;
    use php\io\Stream;
    use php\lang\Module;
    use php\lib\str;
    use php\swing\SwingUtilities;
    use php\swing\Timer;
    use php\swing\UIDialog;
    use php\swing\UIManager;

    const DEVELOPER_VERSION = true;

    // utils functions
    $module = new Module(Stream::of('res://functions.php'));
    $module->call();

    // Manager
    $module = new Module(Stream::of('res://develnext/Bootstrap.php'));
    $module->call();

    $initializer = new Bootstrap();
    $initializer->showSplash();

    SwingUtilities::invokeLater(function() use ($initializer) {
        $initializer->start();
    });
}
