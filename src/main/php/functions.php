<?php

// UTIL Functions
use develnext\DN;
use develnext\ide\IdeManager;
use develnext\ide\ImageManager;
use develnext\IDEForm;
use php\io\File;
use php\lang\Environment;
use php\lang\Thread;
use php\lib\str;
use php\swing\Image;
use php\swing\UIDialog;
use develnext\Bootstrap;

define('IS_WIN', str::startsWith(PHP_OS, 'Windows'));
define('ROOT', (new File("."))->getCanonicalPath());

function dump($var) {
    if ($var instanceof Image) {
        $form = new UIDialog();
        $form->modalType = 'APPLICATION_MODAL';

        $image = new \php\swing\UIImage();
        $image->setImage($var);
        $image->autosize = true;
        $image->position = [0, 0];
        $image->align = 'client';
        $image->centered = true;

        $form->add($image);

        $form->title = 'Debug';

        $form->size = [300, 300];

        $form->moveToCenter();
        $form->show();

        return;
    }

    $text = print_r($var, true);
    UIDialog::message($text, 'Debug');
}

function vdump($var) {
    ob_start();
    var_dump($var);
    $text = ob_get_contents();
    ob_end_clean();

    UIDialog::message($text, 'Var Dump');
}

function _($code, array $args = []) {
    return DN::i18n()->get($code, $args);
}

function __($text) {
    return DN::i18n()->translate($text);
}

