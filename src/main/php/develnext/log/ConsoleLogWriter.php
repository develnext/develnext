<?php
namespace develnext\log;

use develnext\Console;

class ConsoleLogWriter extends LogWriter
{

    public function log($message, $level)
    {
        switch ($level) {
            case LogManager::LEVEL_INFO:
                Console::output($message, 'blue');
                break;

            case LogManager::LEVEL_DEBUG:
                Console::output($message, 'dark gray');
                break;

            case LogManager::LEVEL_WARNING:
                Console::output($message, 'yellow');
                break;

            case LogManager::LEVEL_ERROR:
                Console::output($message, 'light red');
                break;

            default:
                Console::output($message);
        }
    }
}