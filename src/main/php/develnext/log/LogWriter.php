<?php
namespace develnext\log;

abstract class LogWriter
{
    /** @var int */
    protected $_level = LogManager::LEVEL_INFO;

    /** @var bool */
    protected $_enabled = true;

    abstract public function log($message, $level);

    /**
     * @param int $level
     */
    public function setLevel($level)
    {
        $this->_level = $level;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->_level;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->_enabled;
    }
}