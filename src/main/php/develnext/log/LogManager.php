<?php
namespace develnext\log;
use php\time\Time;
use php\time\TimeZone;

/**
 * Class LogManager
 * @package develnext\ide
 */
class LogManager
{
    const LEVEL_ERROR   = 400;
    const LEVEL_WARNING = 300;
    const LEVEL_INFO    = 200;
    const LEVEL_DEBUG   = 100;

    protected static $_labels = [
        self::LEVEL_DEBUG => 'DEBUG',
        self::LEVEL_INFO  => 'INFO',
        self::LEVEL_WARNING => 'WARNING',
        self::LEVEL_ERROR => 'ERROR'
    ];

    /** @var LogWriter[] */
    protected $logWriters = [];

    protected function _log($message, $level)
    {
        $label = self::$_labels[$level];

        $now = Time::now()->toString('H:m:s');
        $message = "[$now] $label: $message\n";

        foreach ($this->logWriters as $writer) {
            if ($writer->isEnabled() && $writer->getLevel() >= $level) {
                $writer->log($message, $level);
            }
        }
    }

    public function addLogWriter(LogWriter $writer, $level = self::LEVEL_INFO)
    {
        $this->logWriters[] = $writer;
        $writer->setLevel($level);
    }

    public function info($message)
    {
        $this->_log($message, self::LEVEL_INFO);
    }

    public function debug($message)
    {
        $this->_log($message, self::LEVEL_DEBUG);
    }

    public function warning($message)
    {
        $this->_log($message, self::LEVEL_WARNING);
    }

    public function error($message)
    {
        $this->_log($message, self::LEVEL_ERROR);
    }
}