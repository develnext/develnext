<?php
namespace develnext\log;

use php\io\Stream;

class StreamLogWriter extends LogWriter
{
    protected $stream;

    function __construct(Stream $stream)
    {
        $this->stream = $stream;
    }

    public function log($message, $level)
    {
        if (!$this->stream->eof()) {
            $this->stream->write($message);
        }
    }
}