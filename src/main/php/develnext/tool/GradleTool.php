<?php
namespace develnext\tool;

use php\io\File;
use php\lib\items;
use php\lib\str;
use php\swing\UIDialog;

class GradleTool extends Tool
{
    public function getVersion()
    {
        return $this->execute(['-v'])->getInput()->readFully();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Gradle';
    }

    /**
     * @return string
     */
    public function getBaseCommand()
    {
        $binPath = 'gradle';

        (new File(\ROOT . '/tools/'))->findFiles(function (File $dir, $name) use (&$binPath) {
            if (str::startsWith($name, 'gradle')) {
                $path = \ROOT . "/tools/{$name}/bin/gradle";

                if (\IS_WIN) {
                    $path .= '.bat';
                }

                if (File::of($path)->exists()) {
                    $binPath = File::of($path)->getPath();
                }
            }
        });

        return $binPath;
    }

    public function execute($directory, array $args = [], $wait = true)
    {
        $jreFile = new File(\ROOT . '/tools/jre');

        if ($jreFile->exists()) {
            $this->environment['JAVA_HOME'] = $jreFile->getPath();
        }

        return parent::execute($directory, items::toList($args, '--daemon'), $wait);
    }
}
