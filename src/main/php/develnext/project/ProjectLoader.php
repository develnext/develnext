<?php
namespace develnext\project;

use develnext\DN;
use develnext\lang\DI;
use ext\xml\DomDocument;
use ext\xml\DomNode;
use ext\xml\XmlProcessor;
use php\format\JsonProcessor;
use php\io\File;
use php\io\IOException;
use php\io\Stream;
use php\lib\str;

/**
 * Class ProjectLoader
 * @package develnext\project
 */
class ProjectLoader
{
    /**
     * @return int
     */
    public function getVersion()
    {
        return Project::VERSION;
    }

    /**
     * @param Project $project
     * @param $extensionClass
     * @param bool $origin
     * @param bool $register
     */
    public function addExtension(Project $project, $extensionClass, $origin = true, $register = true)
    {
        if (!$project->hasExtension($extensionClass) && class_exists($extensionClass)) {
            $extension = new $extensionClass($project);

            if ($extension instanceof ProjectExtension) {
                $project->addExtension($extension, $origin);

                $extension->load();

                if ($register) {
                    $extension->_onRegister();
                }

                foreach ($extension->getExtensions() as $extClass) {
                    $this->addExtension($project, $extClass, false, $register);
                }
            }
        }
    }

    /**
     * @param Project $project
     * @param DomDocument $data
     * @param bool $fully
     */
    public function loadProject(Project $project, DomDocument $data, $fully = true)
    {
        $project->setName($data->get('/project/name'));

        if ($fully) {
            $this->loadFileMarks($project, $data);
            $this->loadExtensions($project, $data, $fully);
        }
    }

    /**
     * @param Project $project
     * @param DomDocument $data
     */
    public function loadExtensions(Project $project, DomDocument $data, $fully = true)
    {
        /** @var DomNode $item */
        foreach ($data->findAll('/project/extensions/item') as $item) {
            $this->addExtension($project, $item->get('type'), true, $fully);
        }
    }

    /**
     * @param Project $project
     * @param DomDocument $data
     */
    public function loadFileMarks(Project $project, DomDocument $data)
    {
        /** @var DomNode $mark */
        foreach ($data->findAll('/project/fileMarks/item') as $mark) {
            $model = $mark->toModel();

            if ($model['@external']) {
                $file = new ProjectFile(new File($model['path'], $project));
            } else {
                $file = $project->getProjectFile($model['path']);
            }

            $project->addFileMark(new FileMark($file, $model['@type']));
        }
    }

    /**
     * @param File $directory
     * @param bool $fully
     * @return Project|null
     */
    public function load(File $directory, $fully = true)
    {
        $processor = new XmlProcessor();

        $dir = new File($directory->getPath() . '/.develnext');
        if (!$dir->isDirectory())
            return null;

        if (!File::of("{$dir}/root.xml")->isFile()) {
            return null;
        }

        try {
            $st = Stream::of("{$dir}/root.xml");
            try {
                $data = $processor->parse($st->readFully());
            } catch (\Exception $e) {
                return null;
            } finally {
                $st->close();
            }
        } catch (IOException $e) {
            return null;
        }

        $projectVersion = (int) $data->get('/project/projectVersion');

        if ($projectVersion && $projectVersion !== Project::VERSION) {
            $st->close();

            $loader = DN::projectManager()->getProjectLoader($projectVersion);
            if ($loader) {
                return $loader->load($directory, $fully);
            }

            return null;
        }

        $template = $data->get('/project/template');

        if ($template && class_exists($template)) {
            $template = new $template();
        } else {
            $template = null;
        }

        $project  = new Project($directory, $template);

        $this->loadProject($project, $data, $fully);

        return $project;
    }
}
