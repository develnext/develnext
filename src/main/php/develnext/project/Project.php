<?php
namespace develnext\project;

use develnext\DN;
use develnext\ide\IdeManager;
use develnext\ide\IdeWizard;
use develnext\Bootstrap;
use develnext\lang\DI;
use php\io\File;
use php\lang\IllegalArgumentException;
use php\lib\items;
use php\lib\mirror;
use php\lib\str;
use php\swing\UIContainer;
use php\swing\UITree;

/**
 * Class Project
 * @package develnext\project
 */
class Project
{
    const VERSION = 20141222;

    /** @var string */
    protected $name;

    /** @var string */
    protected $version;

    /** @var ProjectTemplate */
    protected $template;

    /** @var ProjectExtension[] */
    protected $extensions;

    /** @var ProjectExtension[] */
    protected $originExtensions;

    /** @var IdeWizard[] */
    protected $wizards;

    /** @var File */
    protected $directory;

    /** @var FileTreeManager */
    protected $fileTree;

    /** @var EditorManager */
    protected $editorManager;

    /** @var FileMark[] */
    protected $fileMarks;

    /** @var ProjectFile[] */
    protected $contentRoots = [];


    function __construct(File $directory, ProjectTemplate $template = null)
    {
        $this->directory     = $directory;
        $this->name          = $directory->getName();
        $this->version       = "1.0";

        $this->fileTree      = new FileTreeManager($this);
        $this->editorManager = new EditorManager($this);

        $this->template      = $template;

        DI::register($this->fileTree);
        DI::register($this->editorManager);
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param string $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @return \php\io\File
     */
    public function getDirectory()
    {
        return $this->directory;
    }

    public function getFileMark(ProjectFile $file, $type)
    {
        $mark = new FileMark($file, $type);
        return $this->fileMarks[$mark->hashCode()];
    }

    public function addFileMark(FileMark $mark)
    {
        $this->fileMarks[$mark->hashCode()] = $mark;
    }

    public function setFileMark(ProjectFile $file, $type)
    {
        $mark = new FileMark($file, $type);
        $this->fileMarks[$mark->hashCode()] = $mark;
    }

    public function deleteFileMark(ProjectFile $file, $type)
    {
        $mark = new FileMark($file, $type);
        unset($this->fileMarks[$mark->hashCode()]);
    }

    /**
     * @param $extensionClass
     * @return bool
     */
    public function hasExtension($extensionClass)
    {
        return isset($this->extensions[str::lower($extensionClass)]);
    }

    /**
     * @param ProjectExtension $extension
     * @param bool $origin
     */
    public function addExtension(ProjectExtension $extension, $origin = true)
    {
        if ($origin) {
            $this->originExtensions[mirror::typeOf($extension, true)] = $extension;
        }

        if (!$this->extensions[mirror::typeOf($extension, true)]) {
            $this->extensions[mirror::typeOf($extension, true)] = $extension;
        }
    }

    /**
     * @return ProjectExtension[]
     */
    public function getOriginExtensions()
    {
        return $this->originExtensions;
    }

    /**
     * @return ProjectExtension[]
     */
    public function getExtensions()
    {
        return $this->extensions;
    }

    /**
     * @return FileMark[]
     */
    public function getFileMarks()
    {
        return $this->fileMarks;
    }

    /**
     * @param $path
     * @return File
     */
    public function getFile($path)
    {
        return new File($this->getPath($path));
    }

    /**
     * @param $path
     * @return ProjectFile
     */
    public function getProjectFile($path)
    {
        return new ProjectFile($this->getFile($path), $this);
    }

    public function getPath($path)
    {
        return $this->directory->getPath() . "/$path";
    }

    /**
     * Updates wizards list of Project
     */
    public function updateWizards()
    {
        foreach (DN::ide()->getWizards() as $wizard) {
            $class = str::lower(get_class($wizard));

            if ($curWizard = $this->wizards[$class]) {
                $wizard = $curWizard;

                if ($wizard->available($this)) {
                    $wizard->onUpdate();
                } else {
                    $wizard->delete();
                    unset($this->wizards[$class]);
                }
            } else {
                if ($wizard->available($this)) {
                    $this->wizards[$class] = $wizard->create($this);
                }
            }
        }
    }

    public function updateTree()
    {
        $this->fileTree->updateAll();
    }

    public function updateFile(ProjectFile $file)
    {
        $this->fileTree->updateFile($file);
        if (!$file->getFile()->exists())
            $this->editorManager->close($file);
    }

    public function setGuiElements(UIContainer $editorContainer, UITree $fileTree)
    {
        $this->fileTree->setTree($fileTree);
        $this->editorManager->setArea($editorContainer);

        $this->fileTree->setEditorManager($this->editorManager);
    }

    public function close()
    {
        DI::unregister($this->fileTree);
        DI::unregister($this->editorManager);

        $this->fileTree->close();
        $this->editorManager->closeAll();
    }

    public function saveAll()
    {
        $this->editorManager->saveAll();

        /** @var ProjectWriter $projectWriter */
        $projectWriter = DI::get(ProjectWriter::class);
        $projectWriter->save($this);
    }

    public function openFile(ProjectFile $file)
    {
        $this->editorManager->open($file);
        $this->fileTree->selectFile($file);
    }

    /**
     * @return \develnext\project\EditorManager
     */
    public function getEditorManager()
    {
        return $this->editorManager;
    }

    /**
     * @return \develnext\project\FileTreeManager
     */
    public function getFileTree()
    {
        return $this->fileTree;
    }

    /**
     * @return ProjectTemplate
     */
    public function getTemplate()
    {
        return $this->template;
    }

    public function addContentRoot(File $path)
    {
        $projectFile = new ProjectFile($path, $this);
        $this->contentRoots[$projectFile->hashCode()] = $projectFile;
    }

    public function removeContentRoot(File $path)
    {
        $projectFile = new ProjectFile($path, $this);
        unset($this->contentRoots[$projectFile->hashCode()]);
    }

    public function getContentRoots()
    {
        return $this->contentRoots;
    }

    public function isContentRoot(File $path)
    {
        $projectFile = new ProjectFile($path, $this);
        return isset($this->contentRoots[$projectFile->hashCode()]);
    }

    public function isExternalFile(File $path)
    {
        return (new ProjectFile($path, $this))->isExternal();
    }

    /**
     * @param IdeWizard $wizard
     * @return IdeWizard
     */
    public function getWizard(IdeWizard $wizard)
    {
        return $this->wizards[str::lower(get_class($wizard))];
    }

    /**
     * @param string $event
     */
    public function trigger($event)
    {
        foreach ($this->getExtensions() as $extension) {
            call_user_func([$extension, '_on' . $event]);
        }
    }
}
