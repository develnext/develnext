<?php
namespace develnext\project;

use develnext\DN;
use develnext\ide\IdeManager;
use php\swing\Image;

abstract class ProjectTemplateCategory
{
    /**
     * @return string
     */
    abstract public function getUid();

    /**
     * @return Image
     */
    abstract public function getIcon();

    /**
     * @return string
     */
    abstract public function getName();

    /**
     * @return string
     */
    abstract public function getDescription();

    /**
     * @return ProjectTemplate[]
     */
    public function getTemplates()
    {
        $result = [];

        foreach (DN::ide()->getProjectTemplates() as $template) {
            if ($template->getCategoryUid() === $this->getUid()) {
                $result[] = $template;
            }
        }

        return $result;
    }
}