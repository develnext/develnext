<?php
namespace develnext\project;

use ext\xml\DomDocument;
use ext\xml\DomNode;
use ext\xml\XmlProcessor;
use php\format\JsonProcessor;
use php\io\File;
use php\io\FileStream;
use php\lib\mirror;
use php\time\Time;
use php\time\TimeZone;

/**
 * Class ProjectWriter
 * @package develnext\project
 */
class ProjectWriter
{
    public function saveExtensions(Project $project, DomDocument $document)
    {
        $projectNode = $document->find('project');

        $extensionsNode = $document->createElement('extensions');
        $projectNode->appendChild($extensionsNode);

        foreach ($project->getOriginExtensions() as $extension) {
            $extensionsNode->appendChild($document->createElement('item', [
                'type' => mirror::typeOf($extension)
            ]));
        }
    }

    public function saveFileMarks(Project $project, DomDocument $document)
    {
        $projectNode = $document->find('project');

        $fileMarksNode = $document->createElement('fileMarks');
        $projectNode->appendChild($fileMarksNode);

        foreach ($project->getFileMarks() as $mark) {
            $itemNode = $document->createElement('item', [
                '@type'     => $mark->getType(),
                '@external' => $mark->getFile()->isExternal(),
                'path'      => $mark->getFile()->isExternal() ? $mark->getFile() : $mark->getFile()->getRelPath(),
            ]);

            $fileMarksNode->appendChild($itemNode);
        }
    }

    public function save(Project $project)
    {
        $project->trigger('BeforeSave');

        $processor = new XmlProcessor();

        $data = $processor->createDocument();

        $dir = new File("{$project->getDirectory()}/.develnext");

        if (!$dir->exists()) {
            $dir->mkdirs();
        }

        $projectNode = $data->createElement('project', [
            'projectVersion'    => Project::VERSION,
            'updated_at'        => Time::millis(),
            'timezone'          => ['id' => TimeZone::getDefault()->getId(), 'offset' => TimeZone::getDefault()->getRawOffset()],

            'name'              => $project->getName(),
            'version'           => $project->getVersion(),
            'template'          => $project->getTemplate() ? mirror::typeOf($project->getTemplate()) : null
        ]);

        $data->appendChild($projectNode);

        $this->saveExtensions($project, $data);
        $this->saveFileMarks($project, $data);

        $stream = new FileStream("{$dir}/root.xml", "w+");
        try {
            $processor->formatTo($data, $stream);
        } finally {
            $stream->close();
        }

        foreach ($project->getExtensions() as $extension) {
            $extension->save();
        }

        $project->trigger('AfterSave');
    }
}
