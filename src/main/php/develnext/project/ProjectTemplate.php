<?php
namespace develnext\project;

use develnext\DN;
use develnext\ide\IdeWizard;
use develnext\ide\ImageManager;
use develnext\ide\wizard\ProjectCreationIdeWizard;
use develnext\Bootstrap;
use php\swing\Image;

abstract class ProjectTemplate
{
    /**
     * @return string
     */
    abstract public function getUid();

    /**
     * @return string
     */
    abstract public function getCategoryUid();

    /**
     * @return Image
     */
    public function getIcon() {
        return DN::image('images/icons/projecttype/app16.png');
    }

    /**
     * @return Image
     */
    public function getBigIcon() {
        return DN::image('images/icons/projecttype/app32.png');
    }

    /**
     * @return string
     */
    abstract public function getName();

    /**
     * @return string
     */
    public function getDescription() {
        return $this->getName();
    }

    /**
     * @return ProjectCreationIdeWizard|null
     */
    public function getCreationWizard()
    {
        return null;
    }

    /**
     * @param Project $project
     * @return mixed
     */
    abstract public function updateProject(Project $project);

    /**
     * @param $directory
     * @return bool
     */
    public function openCreationWizard($directory)
    {
        /** @var ProjectCreationIdeWizard $wizard */
        $wizard = $this->getCreationWizard();

        if ($wizard) {
            $wizard = DN::wizard($wizard);
            $wizard->setProject($directory);

            return $wizard->openModal();
        } else {
            return true;
        }
    }

    protected function addExtension(Project $project, $extensionClass)
    {
        DN::projectManager()->addExtension($project, $extensionClass);
    }
}