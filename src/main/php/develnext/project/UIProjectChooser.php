<?php

namespace develnext\project;

use develnext\DN;
use develnext\ide\components\__File;
use develnext\ide\components\UIDirectoryChooser;
use develnext\ide\ImageManager;
use develnext\Bootstrap;
use develnext\lang\DI;
use php\io\File;
use php\swing\Font;
use php\swing\tree\TreeNode;
use php\swing\UIDialog;
use php\swing\UILabel;
use php\swing\UITree;

class UIProjectChooser extends UIDirectoryChooser
{
    public function __construct($forSave = false)
    {
        parent::__construct('project' . ($forSave ? 'Save' : 'Load'));
        $this->setOnlyDirectories(true);

        $this->onFetchIcon(function (UIDirectoryChooser $self, File $file) {
            /** @var ProjectLoader $loader */
            $loader = DI::get(ProjectLoader::class);
            $project = $loader->load($file, false);

            if ($project) {
                return $project->getTemplate() ? $project->getTemplate()->getIcon() : null;
            }
        });

        if (!$forSave) {
            $this->setOkButtonEnabled(false);

            $this->onSelected(function (UIDirectoryChooser $self, File $file) {
                /** @var ProjectLoader $loader */
                $loader = DI::get(ProjectLoader::class);
                $project = $loader->load($file, false);
                $self->setOkButtonEnabled(!!$project);
            });
        } else {
            $this->setOkButtonEnabled(true);

            $this->onSelected(function (UIDirectoryChooser $self, File $file) {
                /** @var ProjectLoader $loader */
                $loader = DI::get(ProjectLoader::class);
                $project = $loader->load($file, false);
                $self->setOkButtonEnabled(!$project);
            });
        }
    }
}
