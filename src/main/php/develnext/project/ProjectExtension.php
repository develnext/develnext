<?php
namespace develnext\project;

use develnext\DN;
use develnext\ide\IdeWizard;
use develnext\util\XmlConfig;
use php\io\File;
use php\io\FileStream;
use php\lib\str;
use php\swing\UIDialog;

abstract class ProjectExtension
{
    /** @var Project */
    protected $project;

    /** @var string[] */
    private $extensions = [];

    /** @var IdeWizard[] */
    protected $wizards = [];

    /** @var XmlConfig */
    protected $config;

    abstract public function getUid();

    function __construct(Project $project)
    {
        $this->project = $project;
    }

    /**
     * @return null|\php\swing\Image
     */
    public function getIcon()
    {
        return DN::image('icons/module16.png');
    }

    /**
     * @return null|\php\swing\Image
     */
    public function getBigIcon()
    {
        return DN::image('icons/module32.png');
    }

    /**
     * @return string[]
     */
    public function getExtensions()
    {
        return $this->extensions;
    }

    protected function addExtension($extensionClass)
    {
        $this->extensions[str::lower($extensionClass)] = $extensionClass;
    }

    protected function registerWizard(IdeWizard $wizard)
    {
        $this->wizards[] = $wizard;
    }

    public function save()
    {
        $this->config->save();
    }

    public function load()
    {
        if ($this->config) {
            $this->config->close();
        }

        $file = new File($this->project->getPath('.develnext') . '/extension.' . $this->getUid() . '.xml');

        $this->config = new XmlConfig(
            $st = new FileStream($file, 'a+')
        );
    }

    // Events.
    public function _onRegister()
    {
        // nop.
    }

    public function _onBeforeLoad()
    {
        // nop.
    }

    public function _onAfterLoad()
    {
        // nop.
    }

    public function _onAfterSave()
    {
        // nop.
    }

    public function _onBeforeSave()
    {
        // nop.
    }

    public function _onBeforeCreate()
    {
        // nop.
    }

    public function _onAfterCreate()
    {
        // nop.
    }

    public function _onBeforeClose()
    {
        // nop.
    }

    public function _onAfterClose()
    {
        // nop.
    }
}