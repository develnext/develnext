<?php
namespace develnext\project;

use develnext\DN;
use develnext\forms\IDEMainForm;
use develnext\ide\wizard\ProjectLocationWizard;
use develnext\lang\DI;
use develnext\lang\Singleton;
use develnext\util\XmlConfig;
use ext\xml\DomNode;
use php\io\File;
use php\io\FileStream;
use php\lang\System;
use php\lib\char;
use php\lib\str;
use php\time\Time;
use php\util\Flow;
use php\util\Scanner;

class ProjectManager
{
    /** @var Project[] */
    protected $_latestProjects;

    /** @var XmlConfig */
    protected $_config;

    /** @var ProjectLoader[] */
    protected $_loaders;

    /** @var File */
    protected $_defaultDirectory;

    function __construct()
    {
        $this->loadConfiguration();

        $this->_loaders['v' . Project::VERSION] = DI::get(ProjectLoader::class);

        $this->_defaultDirectory = new File(DN::config()->userHome(), '/DevelNextProjects');
    }

    /**
     * @param Project $project
     * @param string $extensionClass
     */
    public function addExtension(Project $project, $extensionClass)
    {
        /** @var ProjectLoader $loader */
        $loader = DI::get(ProjectLoader::class);
        $loader->addExtension($project, $extensionClass);
    }

    /**
     * @param int $version
     * @return ProjectLoader
     */
    public function getProjectLoader($version)
    {
        if ($loader = $this->_loaders['v' . $version]) {
            return $loader;
        }

        /** @var ProjectLoader $loader */
        foreach (DI::findInstanceOf(ProjectLoader::class) as $loader) {
            if ($loader->getVersion() === $version) {
                return $loader;
            }
        }

        return null;
    }

    /**
     * @return Project[]
     */
    public function getLatestProjects()
    {
        return $this->_latestProjects;
    }

    /**
     * @param $index
     * @return Project
     */
    public function getLatestProject($index)
    {
        return $this->_latestProjects[$index];
    }

    protected function addToLatest(Project $project)
    {
        $index = null;

        foreach ($this->_latestProjects as $index => $file) {
            if ($file->getDirectory()->getPath() === $project->getDirectory()->getPath()) {
                break;
            }
            $index = null;
        }

        if ($index !== null) {
            unset($this->_latestProjects[$index]);
        }

        $this->_latestProjects = Flow::of([$project])->append($this->_latestProjects)->limit(15)->toArray();
        $this->saveConfiguration();
    }

    /**
     * @param ProjectTemplate $projectTemplate
     * @return Project
     */
    public function createProject(ProjectTemplate $projectTemplate = null)
    {
        $name = 'Project';
        $defaultPath = $this->_defaultDirectory;

        if ($projectTemplate) {
            $defaultPath = new File($this->_defaultDirectory, '/' . $projectTemplate->getUid());
        }

        $directory = new File($defaultPath, '/' . $name);

        $i = 1;

        while ($directory->exists()) {
            $directory = new File($defaultPath, "/{$name} " . (++$i));
        }

        $result = DN::wizard(ProjectLocationWizard::class)->openModal(['name' => $name, 'directory' => $directory]);

        if (!$result) {
            return null;
        }

        $name      = $result['name'];
        $directory = new File("{$result['directory']}");

        if (!$projectTemplate || $projectTemplate->openCreationWizard($directory)) {
            $this->closeProject();

            if (!$directory->exists())
                $directory->mkdirs();

            $project = new Project($directory, $projectTemplate);

            if ($projectTemplate) {
                $projectTemplate->updateProject($project);
            }

            DI::register($project);

            $project->trigger('BeforeCreate');

            $project->setName($name);
            $this->_linkGuiToProject($project);

            $project->setFileMark($project->getProjectFile('.develnext'), 'hidden');

            $project->saveAll();
            $project->updateWizards();

            $this->addToLatest($project);

            $project->trigger('AfterCreate');

            $project->updateTree();
            return $project;
        } else {
            return null;
        }
    }

    public function openProject(File $directory)
    {
        /** @var ProjectLoader $loader */
        $loader = DI::get(ProjectLoader::class);

        $project = $loader->load($directory);

        $project->trigger('BeforeLoad');

        if (!$project)
            return null;

        DI::register($project);

        $this->_linkGuiToProject($project);

        $project->setFileMark($project->getProjectFile('.develnext'), 'hidden');

        $project->saveAll();
        $project->updateWizards();

        $this->addToLatest($project);

        $project->trigger('AfterLoad');

        $project->updateTree();
        return $project;
    }


    public function closeProject()
    {
        $project = DN::project();

        if ($project) {
            $project->trigger('BeforeClose');
            $project->close();
            $project->trigger('AfterClose');

            DI::unregister($project);
        }
    }

    public function loadConfiguration()
    {
        $this->_latestProjects = [];

        /** @var ProjectLoader $loader */
        $loader = DI::get(ProjectLoader::class);

        if (!$this->_config) {
            $this->_config = $config = DN::config()->xml('latestProjects');
        } else {
            $config = $this->_config;
        }

        /** @var DomNode[] $projects */
        $projects = $config->getDocument()->findAll('/projects/project');

        foreach ($projects as $project) {
            $model = $project->toModel();

            $path = new File($model['path']);

            if ($path->isDirectory()) {
                $pr = $loader->load($path, false);

                if ($pr) {
                    $this->_latestProjects[] = $pr;
                }
            }
        }
    }

    public function saveConfiguration()
    {
        $projects = $this->_config->getDocument()->find('/projects');

        if ($projects) {
            $this->_config->getDocument()->removeChild($projects);
        }

        $projects = $this->_config->getDocument()->createElement('projects');
        $this->_config->getDocument()->appendChild($projects);

        foreach ($this->_latestProjects as $el) {
            $project = $this->_config->getDocument()->createElement(
                'project',
                [
                    'path'  => $el->getDirectory(),
                    '@user' => System::getProperty('user.name'),
                    '@time' => Time::seconds()
                ]
            );

            $projects->appendChild($project);
        }

        $this->_config->save();
    }

    protected function _linkGuiToProject(Project $project)
    {
        $form = DN::ide()->getMainForm();
        $project->setGuiElements($form->area, $form->fileTree);
    }
}
