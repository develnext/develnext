<?php

namespace develnext\project\loaders;

use develnext\project\ProjectLoader;
use php\io\File;

/**
 * Example class ....
 *
 * Class ProjectLoader20121221
 * @package develnext\project\loaders
 */
class ProjectLoader20121221 extends ProjectLoader
{
    public function getVersion()
    {
        return 20121221;
    }

    public function load(File $directory, $fully = true)
    {
        return parent::load($directory, $fully);
    }
}