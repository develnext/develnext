<?php
namespace develnext\ui;

use php\lib\mirror;
use php\swing\Color;
use php\swing\Font;
use php\swing\UIDialog;
use php\swing\UIElement;
use php\swing\UILabel;
use php\swing\UIPanel;
use php\swing\UIWindow;

class UIToolTip extends UILabel
{
    /** @var UILabel */
    protected $text;

    public function __construct()
    {
        parent::__construct();

        $this->horAlignment = 'center';
        $this->verAlignment = 'center';

        $this->on('click', function() {
            $this->hide();
        });
    }

    public function setBackground($color)
    {
        $this->background = $color;
    }

    public function setColor($color)
    {
        $this->foreground = $color;
    }

    public function setFont(Font $font)
    {
        $this->font = $font;
    }

    public function setText($text)
    {
        $this->text = $text;
    }

    protected function _updateSizes()
    {
        $this->w = $this->parent->getTextWidth($this->text) + 20;
        $this->h = $this->parent->getTextHeight() + 20;
    }

    public function showText($text)
    {
        $this->setText($text);
        $this->show();
    }

    static function error($text, UIElement $element)
    {
        $tooltip = new UIToolTip();

        $tooltip->setColor(Color::rgb(255, 0, 0));

        $tooltip->position = $element->position;
        $tooltip->y += $element->h + 3;

        $parent = $element->parent;

        /*while ($parent != null) {
            if ($parent instanceof UIWindow) {
                break;
            }

            $parent = $parent->parent;
        }*/

        $parent->add($tooltip);
        $tooltip->showText($text);
        $tooltip->_updateSizes();

        dump($tooltip);
    }
}