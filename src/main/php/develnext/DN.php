<?php
namespace develnext;

use develnext\i18n\Localizer;
use develnext\ide\ConfigManager;
use develnext\ide\IdeManager;
use develnext\ide\IdeWizard;
use develnext\ide\ImageManager;
use develnext\log\LogManager;
use develnext\lang\DI;
use develnext\project\Project;
use develnext\project\ProjectManager;
use develnext\validation\ValidationResult;
use develnext\validation\ValidationRules;
use develnext\validation\Validator;
use ext\javafx\UXStage;
use php\lib\mirror;
use php\swing\Image;

/**
 * Class DN
 * @package develnext
 */
final class DN
{
    private function __construct() {}

    /**
     * @return Bootstrap
     */
    static function bootstrap()
    {
        return DI::get(Bootstrap::class);
    }

    /**
     * @return ProjectManager
     */
    static function projectManager()
    {
        return DI::get(ProjectManager::class);
    }

    /**
     * @return IdeManager
     */
    static function ide()
    {
        return DI::get(IdeManager::class);
    }

    /**
     * @param string $event
     * @param array $args
     */
    static function trigger($event, array $args = [])
    {
        self::ide()->trigger($event, $args);
    }

    /**
     * @return null|Localizer
     */
    static function i18n()
    {
        return DI::get(Localizer::class);
    }

    /**
     * @return LogManager
     */
    static function log()
    {
        return DI::get(LogManager::class);
    }

    /**
     * Alias of Project::current()
     * @return Project
     */
    static function project()
    {
        return DI::get(Project::class, false);
    }

    /**
     * @return ConfigManager
     */
    static function config() {
        return DI::get(ConfigManager::class);
    }

    /**
     * @param $href
     * @throws \Exception
     */
    static function open($href)
    {
        return self::ide()->openHref($href);
    }

    /**
     * @param $path
     * @param bool $cached
     * @return UXStage
     */
    static function stage($path, $cached = true)
    {
        return self::ide()->getStage($path, $cached);
    }

    /**
     * @param $path
     * @param bool $cached
     * @return IDEForm
     */
    static function form($path, $cached = true)
    {
        return self::ide()->getForm($path, $cached);
    }

    /**
     * @return ImageManager
     */
    static function images()
    {
        return DI::get(ImageManager::class);
    }

    /**
     * @param $path
     * @return null|Image
     */
    static function image($path)
    {
        return self::images()->get($path);
    }

    /**
     * @param string|IdeWizard $wizardClassOrObject
     * @return ide\IdeWizard
     */
    static function wizard($wizardClassOrObject)
    {
        if (is_object($wizardClassOrObject)) {
            $wizardClassOrObject = mirror::typeOf($wizardClassOrObject);
        }

        /** @var IdeWizard $wizard */
        $wizard = DI::get($wizardClassOrObject);

        $project = self::project();

        if (!($wizard || !$wizard->available(null)) && $project) {
            $wizard = $project->getWizard(new $wizardClassOrObject);
        }

        return $wizard;
    }

    /**
     * @param array|object $data
     * @param ValidationRules $rules
     * @return ValidationResult
     */
    static function validate($data, ValidationRules $rules)
    {
        return DI::get(Validator::class)->validate($data, $rules);
    }
}