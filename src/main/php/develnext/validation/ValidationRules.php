<?php
namespace develnext\validation;

use develnext\lang\DI;
use php\lib\items;
use php\util\Flow;
use ReflectionClass;

class ValidationRules
{
    public function validate($data, ValidationResult $result)
    {
        /** @var Validator $validator */
        $validator = DI::get(Validator::class);

        $class = new ReflectionClass($this);

        foreach ($class->getProperties(\ReflectionProperty::IS_PUBLIC) as $property)
        {
            $name  = $property->getName();
            $attrs = $property->getValue($this);

            $value = $data[$name];

            foreach ($attrs as $key => $item) {
                $params = [];
                $uid    = $item;

                if (is_array($item)) {
                    $params = $item;
                    $uid    = $key;
                }

                $validatorType = $validator->getValidatorType($uid);

                if (!$validatorType) {
                    throw new \Exception("Cannot find validator with uid = '$uid''");
                }

                $message = $params['message'];

                if (!$message) {
                    $message = $validatorType->getDefaultMessage($params);
                }

                if (!$validatorType->validate($value, $params)) {
                    $result->addError(
                        $name,
                        $uid,
                        _($message, Flow::of(['name' => $name])->append($params)->withKeys()->toArray())
                    );
                }
            }
        }
    }
}