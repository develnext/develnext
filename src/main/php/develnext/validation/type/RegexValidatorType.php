<?php

namespace develnext\validation\type;

use develnext\validation\ValidatorType;
use php\util\Regex;

class RegexValidatorType extends ValidatorType
{
    /**
     * @return string
     */
    public function getUid()
    {
        return "regex";
    }

    /**
     * @param array $params
     * @return string
     */
    public function getDefaultMessage(array $params)
    {
        return 'The value is not valid';
    }

    /**
     * @param $value
     * @param array $params
     * @return string
     */
    public function validate($value, array $params)
    {
        return Regex::match($params['pattern'], $value);
    }

    public function getRequiredParams()
    {
        return ['pattern'];
    }
}