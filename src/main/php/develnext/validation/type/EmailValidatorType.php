<?php
namespace develnext\validation\type;

use develnext\validation\ValidatorType;
use php\util\Regex;

class EmailValidatorType extends ValidatorType
{
    const PATTERN = '^([a-z0-9_\.-]+)@([a-z0-9_\.-]+)\.([a-z\.]{2,6})$';
    /**
     * @return string
     */
    public function getUid()
    {
        return "email";
    }

    /**
     * @param array $params
     * @return string
     */
    public function getDefaultMessage(array $params)
    {
        return 'This is not valid email';
    }

    /**
     * @param $value
     * @param array $params
     * @return string
     */
    public function validate($value, array $params)
    {
        return Regex::match(self::PATTERN, $value);
    }
}