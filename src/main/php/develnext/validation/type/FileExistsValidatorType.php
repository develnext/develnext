<?php

namespace develnext\validation\type;

use develnext\validation\ValidatorType;
use php\io\File;

class FileExistsValidatorType extends ValidatorType
{
    /**
     * @return string
     */
    public function getUid()
    {
        return "fileExists";
    }

    /**
     * @param array $params
     * @return string
     */
    public function getDefaultMessage(array $params)
    {
        return 'The file is not exists';
    }

    /**
     * @param $value
     * @param array $params
     * @return string
     */
    public function validate($value, array $params)
    {
        if ($params['onlyFiles']) {
            return File::of($value)->isFile();
        } else if ($params['onlyDirectories']) {
            return File::of($value)->isDirectory();
        } else {
            return File::of($value)->exists();
        }
    }
}