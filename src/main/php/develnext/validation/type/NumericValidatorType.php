<?php
namespace develnext\validation\type;

use develnext\validation\ValidatorType;
use php\lib\str;
use php\util\Regex;

/**
 * Class NumericValidatorType
 * @package develnext\validation\type
 */
class NumericValidatorType extends ValidatorType {
    /**
     * @return string
     */
    public function getUid()
    {
        return "numeric";
    }

    /**
     * @return string
     */
    public function getDefaultMessage(array $params)
    {
        if ($params['float']) {
            return 'The value must be an float';
        } else {
            return 'The value must be an integer';
        }
    }

    /**
     * @param $value
     * @param array $params
     * @return string
     */
    public function validate($value, array $params)
    {
        if ($params['float']) {
            return Regex::match('[-+]?([0-9]*\\.[0-9]+|[0-9]+)', $value);
        } else {
            return str::isNumber($value);
        }
    }
}