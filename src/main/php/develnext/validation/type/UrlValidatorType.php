<?php
namespace develnext\validation\type;

use develnext\validation\ValidatorType;
use php\util\Regex;

class UrlValidatorType extends ValidatorType
{
    const PATTERN = '^(https?:\\/\\/)?([\\da-z\\.-]+)\\.([a-z\\.]{2,6})([\\/\w \\.-]*)*\\/?$';

    /**
     * @return string
     */
    public function getUid()
    {
        return "url";
    }

    /**
     * @param array $params
     * @return string
     */
    public function getDefaultMessage(array $params)
    {
        return 'The value is not valid url';
    }

    /**
     * @param $value
     * @param array $params
     * @return string
     */
    public function validate($value, array $params)
    {
        return Regex::match(self::PATTERN, $value);
    }
}