<?php

namespace develnext\validation\type;

use develnext\validation\ValidatorType;
use php\io\File;

class PathValidatorType extends ValidatorType
{
    /**
     * @return string
     */
    public function getUid()
    {
        return "path";
    }

    /**
     * @param array $params
     * @return string
     */
    public function getDefaultMessage(array $params)
    {
        return 'The value is not valid path';
    }

    /**
     * @param $value
     * @param array $params
     * @return string
     */
    public function validate($value, array $params)
    {
        return $value && File::of($value)->getCanonicalFile();
    }
}