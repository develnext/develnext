<?php
namespace develnext\validation\type;

use develnext\validation\ValidatorType;
use php\lib\str;

class RequiredValidatorType extends ValidatorType
{
    public function getUid()
    {
        return "required";
    }

    /**
     * @return string
     */
    public function getDefaultMessage(array $params)
    {
        return 'The value is required';
    }

    public function validate($value, array $params)
    {
        return !!str::trim($value);
    }
}