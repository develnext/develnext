<?php
namespace develnext\validation;

/**
 * Class ValidatorType
 * @package develnext\validation
 */
abstract class ValidatorType
{
    /**
     * @return string
     */
    abstract public function getUid();

    /**
     * @param array $params
     * @return string
     */
    abstract public function getDefaultMessage(array $params);

    /**
     * @param $value
     * @param array $params
     * @return string
     */
    abstract public function validate($value, array $params);

    /**
     * @return array
     */
    public function getRequiredParams()
    {
        return [];
    }
}