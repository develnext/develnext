<?php

namespace develnext\validation;


use php\lib\items;
use php\lib\str;

class ValidationResult
{
    /** @var array */
    protected $_errors = [];

    public function addError($name, $uid, $message)
    {
        $this->_errors[$name][str::lower($uid)][] = $message;
    }

    public function hasErrors()
    {
        return (bool) $this->_errors;
    }

    public function getErrors()
    {
        $result = [];

        foreach ($this->_errors as $name => $item) {
            $messages = [];

            foreach ($item as $type => $message) {
                $messages = items::toList($messages, $message);
            }

            $result[$name] = $messages;
        }

        return $result;
    }
}
