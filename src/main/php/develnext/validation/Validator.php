<?php
namespace develnext\validation;

use develnext\validation\type\EmailValidatorType;
use develnext\validation\type\FileExistsValidatorType;
use develnext\validation\type\NumericValidatorType;
use develnext\validation\type\PathValidatorType;
use develnext\validation\type\RegexValidatorType;
use develnext\validation\type\RequiredValidatorType;
use develnext\validation\type\UrlValidatorType;
use php\lib\mirror;
use php\lib\str;

class Validator
{
    /** @var ValidatorType[] */
    protected $_validationTypes = [];

    function __construct()
    {
        $this->registerValidatorType(new RequiredValidatorType());
        $this->registerValidatorType(new EmailValidatorType());
        $this->registerValidatorType(new UrlValidatorType());
        $this->registerValidatorType(new NumericValidatorType());
        $this->registerValidatorType(new PathValidatorType());
        $this->registerValidatorType(new RegexValidatorType());
        $this->registerValidatorType(new FileExistsValidatorType());
    }

    public function registerValidatorType(ValidatorType $type)
    {
        if ($this->_validationTypes[str::lower($type->getUid())]) {
            throw new \Exception("ValidatorType[{$type->getUid()}] already registered");
        }

        $this->_validationTypes[str::lower($type->getUid())] = $type;
    }

    /**
     * @param string $uid
     * @return ValidatorType
     */
    public function getValidatorType($uid)
    {
        return $this->_validationTypes[str::lower($uid)];
    }

    /**
     * @param $data
     * @param ValidationRules $rules
     * @return ValidationResult
     * @throws \Exception
     */
    public function validate($data, ValidationRules $rules)
    {
        $result = new ValidationResult();

        $rules->validate($data, $result);

        return $result;
    }
}