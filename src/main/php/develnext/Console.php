<?php
namespace develnext;

use php\lang\IllegalArgumentException;

/**
 * Class Console
 * @package develnext
 */
class Console
{
    private static $foregroundColors = [
        'black' => '0;30',
        'dark gray' => '1;30',
        'blue' => '0;34',
        'light blue' => '1;34',
        'green' => '0;32',
        'light green' => '1;32',
        'cyan' => '0;36',
        'light cyan' => '1;36',
        'red' => '0;31',
        'light red' => '1;31',
        'purple' => '0;35',
        'light purple' => '1;35',
        'brown' => '0;33',
        'yellow' => '1;33',
        'light_gray' => '0;37',
        'white' => '1;37'
    ];

    public static function output($text, $color = 'black')
    {
        if (!isset(self::$foregroundColors[$color])) {
            throw new IllegalArgumentException("Color '$color' is invalid");
        }

        echo $text;
    }
}