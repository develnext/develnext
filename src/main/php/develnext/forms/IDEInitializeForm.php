<?php
namespace develnext\forms;

use develnext\DN;
use develnext\IDEForm;
use php\lang\Module;
use php\swing\UIButton;
use php\swing\UIListbox;
use php\swing\UIWindow;

/**
 * Class IDEInitializeForm
 * @package develnext\forms
 *
 * @property UIListbox $languagesList
 * @property UIButton $nextBtn
 */
class IDEInitializeForm extends IDEForm
{
    function __construct(UIWindow $window, Module $module = null, array $vars = [])
    {
        parent::__construct($window, $module, $vars);
    }
}