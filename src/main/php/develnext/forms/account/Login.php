<?php

/** @var \develnext\IDEForm $form */

use develnext\DN;
use develnext\Bootstrap;

$form->get('btn-login')->on('click', function() use ($form) {
    $form->hide(true);
});

$form->get('btn-exit')->on('click', function() use ($form) {
     $form->hide(false);
});

$form->get('btn-reg')->on('click', function() {
    DN::form('account/Registration')->showModal();
});
