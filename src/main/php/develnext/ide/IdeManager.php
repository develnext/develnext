<?php
namespace develnext\ide;

use develnext\DN;
use develnext\filetype\creator\FileTypeCreator;
use develnext\filetype\FileType;
use develnext\forms\IDEMainForm;
use develnext\i18n\Localizer;
use develnext\IDEForm;
use develnext\lang\DI;
use develnext\Bootstrap;
use develnext\project\Project;
use develnext\project\ProjectTemplate;
use develnext\project\ProjectTemplateCategory;
use develnext\tool\Tool;
use develnext\ui\UITabHead;
use ext\javafx\UXLoader;
use ext\javafx\UXStage;
use php\io\File;
use php\io\IOException;
use php\io\MemoryStream;
use php\io\Stream;
use php\lang\IllegalArgumentException;
use php\lang\Module;
use php\lang\Process;
use php\lang\Thread;
use php\lib\items;
use php\lib\str;
use php\swing\Color;
use php\swing\event\SimpleEvent;
use php\swing\Image;
use php\swing\SwingUtilities;
use php\swing\SwingWorker;
use php\swing\text\Style;
use php\swing\UIButton;
use php\swing\UIContainer;
use php\swing\UIElement;
use php\swing\UILabel;
use php\swing\UIMenu;
use php\swing\UIMenuBar;
use php\swing\UIMenuItem;
use php\swing\UIPanel;
use php\swing\UIPopupMenu;
use php\swing\UIReader;
use php\swing\UIRichTextArea;
use php\swing\UITabs;
use php\swing\UITextArea;
use php\util\Promise;
use php\util\Scanner;

/**
 * Class IdeManager
 * @package develnext\ide
 */
class IdeManager
{
    /** @var UIPopupMenu */
    protected $_fileTreeMenu;

    /** @var IdeExtension[] */
    protected $_extensions = [];

    /** @var FileTypeCreator */
    protected $_fileTypeCreatorsInMenu = [];

    /** @var IdeTool[] */
    protected $_tools = [];

    /** @var IdeBackgroundTask[] */
    protected $_backgroundTasks;

    /** @var callable[] */
    protected $_handlers = [];

    /** @var IdeWizard[] */
    protected $_wizards;

    /** @var ProjectTemplate[] */
    protected $_projectTemplates = [];

    /** @var ProjectTemplateCategory[] */
    protected $_projectTemplateCategories = [];

    /** @var array */
    protected $_hrefProtocolHandlers = [];

    /** @var array */
    protected $_snapshot = [];

    /** @var array */
    protected $_cachedForms = [];

    public function __construct()
    {
        // popup
        $this->_fileTreeMenu = new UIPopupMenu();
    }

    public function newSnapshot()
    {
        $this->_snapshot = [];
    }

    public function applySnapshot()
    {
        /** @var FileType $fileType */
        foreach ($this->_snapshot['fileTypes'] as $fileType) {
            $this->unregisterFileType($fileType);
        }

        /** @var FileTypeCreator $creator */
        foreach ($this->_snapshot['fileTypeCreators'] as $creator) {
            $this->unregisterFileTypeCreator($creator);
        }

        /** @var IdeWizard $ideWizard */
        foreach ($this->_snapshot['ideWizards'] as $ideWizard) {
            $this->unregisterIdeWizard($ideWizard);
        }

        $this->newSnapshot();
    }

    /**
     * @param IdeExtension $extension
     */
    public function registerExtension(IdeExtension $extension)
    {
        $extension->onRegister($this);
        $this->_extensions[] = $extension;

        DI::register($extension);
    }

    /**
     * @param $extensionClass
     * @return IdeExtension|null
     */
    public function getExtension($extensionClass)
    {
        $extensionClass = str::lower($extensionClass);

        foreach ($this->_extensions as $extension) {
            if (str::lower(get_class($extension)) === $extensionClass)
                return $extension;
        }

        return null;
    }

    public function registerFileType(FileType $fileType)
    {
        DI::register($fileType);

        $this->_snapshot['fileTypes'][] = $fileType;
    }

    public function unregisterFileType(FileType $fileType)
    {
        DI::unregister($fileType);
    }

    public function registerFileTypeCreator(FileTypeCreator $creator, $inMenu = true)
    {
        if ($inMenu) {
            $item = $this->addFileTreePopupItem('new', '', $creator->getDescription(), $creator->getIcon());
            $this->_fileTypeCreatorsInMenu[$item->uid] = $creator;

            $item->on('click', function () use ($creator) {
                $creator->open(DN::project()->getFileTree()->getCurrentFile());
            });
        }

        $this->_snapshot['fileTypeCreators'][] = $creator;

        DI::register($creator);
    }

    public function unregisterFileTypeCreator(FileTypeCreator $creator)
    {
        DI::unregister($creator);

        foreach ($this->_fileTypeCreatorsInMenu as $uid => $creator) {
            if ($creator === $creator) {
                $uiElement = UIElement::getByUid($uid);

                if ($uiElement) {
                    $uiElement->owner->remove($uiElement);
                }

                unset($this->_fileTypeCreatorsInMenu[$uid]);
            }
        }
    }

    public function registerIdeTool($code, IdeTool $tool)
    {
        $this->_tools[$code] = $tool;
        DI::register($tool);
    }

    public function unRegisterIdeTool($code)
    {
        DI::unregister($this->_tools[$code]);
        unset($this->_tools[$code]);
    }

    public function registerProjectTemplate(ProjectTemplate $template)
    {
        $this->_projectTemplates[$template->getUid()] = $template;

        $creationWizard = $template->getCreationWizard();

        if ($creationWizard) {
            $this->registerIdeWizard($creationWizard);
        }

        DI::register($template);
    }

    public function registerProjectTemplateCategory(ProjectTemplateCategory $category)
    {
        $this->_projectTemplateCategories[$category->getUid()] = $category;
        DI::register($category);
    }

    public function registerIdeWizard(IdeWizard $wizard)
    {
        $this->_wizards[str::lower(get_class($wizard))] = $wizard;
        DI::register($wizard);

        $this->_snapshot['ideWizards'][] = $wizard;

        if ($wizard->available(null)) {
            $wizard->create();
        }
    }

    public function unregisterIdeWizard(IdeWizard $wizard)
    {
        unset($this->_wizards[str::lower(get_class($wizard))]);
        DI::unregister($wizard);
    }

    public function registerHrefProtocolHandler($protocol, callable $handler)
    {
        $this->_hrefProtocolHandlers[$protocol] = $handler;
    }

    public function openHref($href)
    {
        $k = str::pos($href, '://');

        if ($k == -1) {
            throw new \Exception("Cannot open '$href'");
        }

        $protocol = str::sub($href, 0, $k);
        $handler  = $this->_hrefProtocolHandlers[$protocol];

        if (!$handler) {
            throw new \Exception("Unsupported protocol '$href'");
        }

        $href  = str::sub($href, $k + 3);
        $href  = str::split($href, '?', 2);

        $query = (string) $href[1];
        $href  = $href[0];

        $params = [];

        foreach (str::split($query, '&') as $el) {
            list($key, $value) = str::split($el, '=');

            if (is_array($params[$key])) {
                $params[$key][] = $value;
            } else if (isset($params[$key])) {
                $params[$key] = [$params[$key], $value];
            } else {
                $params[$key] = $value;
            }
        }

        return $handler($href, $params);
    }

    public function closeTool(IdeTool $tool, $withTrigger = true)
    {
        if (!$tool->getUiTab())
            throw new IllegalArgumentException("Tool '" . $tool->getName() . "' is not opened");

        /** @var UITabs $toolTabs */
        $toolTabs = $this->getMainForm()->get('tool-tabs');

        if (!$withTrigger || $tool->trigger('close')) {
            $toolTabs->remove($tool->getUiTab());
            if (!$toolTabs->tabCount) {
                $this->getMainForm()->getDockTools()->visible = false;
            }
        }
    }

    public function openTool($code, $title = null, $icon = null)
    {
        /** @var IdeTool $tool */
        $tool = $this->_tools[$code];
        if ($tool == null) {
            throw new IllegalArgumentException("Ide Tool '$code' is not registered'");
        }
        // copy
        $tool = clone $tool;

        $content = $tool->createGui($this);
        /** @var UITabs $toolTabs */
        $toolTabs = $this->getMainForm()->get('tool-tabs');

        $tab = new UIPanel();
        $tab->add($content);

        $toolTabs->add($tab);
        $toolTabs->setTabComponentAt(
            $index = $toolTabs->tabCount - 1,
            $tabHead = new UITabHead(
                $toolTabs, $tab, $title === null ? $tool->getName() : $title,
                $icon === null ? DN::image($tool->getIcon()) : $icon
            )
        );

        $tabHead->on('close', function () use ($tool) {
            $this->closeTool($tool, true);
        });

        $toolTabs->selectedComponent = $tab;
        $this->getMainForm()->getDockTools()->visible = true;
        $this->getMainForm()->getDockTools()->setExtendedMode('normalized');

        $tool->setUiTab($tab);
        $tool->setUiTabHead($tabHead);
        return $tool;
    }

    /**
     * @param int $size
     * @return UIPanel
     */
    public function addHeadMenuGap($size = 4)
    {
        $menu = $this->getMainForm()->get('headMenu');
        if ($menu) {
            $gap = new UIPanel();
            $gap->align = 'left';
            $gap->w = $size;
            $menu->add($gap);
            return $gap;
        }
    }

    public function addHeadMenuSeparator($size = 5)
    {
        $this->addHeadMenuGap($size);

        $menu = $this->getMainForm()->get('headMenu');
        if ($menu) {
            $sep = new UIPanel();
            $sep->align = 'left';
            $sep->w = 1;
            $sep->background = Color::decode('#9E9E9E');
            $menu->add($sep);
        }

        $this->addHeadMenuGap($size);
    }

    /**
     * @param UIElement $element
     * @return UIElement[] result ul elements of adding
     */
    public function addHeadMenuAny(UIElement $element)
    {
        $menu = $this->getMainForm()->get('headMenu');
        if ($menu) {
            $element->align = 'left';
            $menu->add($element);
            $gap = $this->addHeadMenuGap(2);

            return [$element, $gap];
        }
    }

    /**
     * @param string $group
     * @return NULL|UIElement
     */
    public function findFromHeadMenu($group)
    {
        /** @var UIContainer $menu */
        $menu = $this->getMainForm()->get('headMenu');
        if ($menu) {
            return $menu->getComponentByGroup($group);
        }
    }

    /**
     * @param $group
     * @param $icon
     * @param string $text
     * @return UIButton
     */
    public function addHeadMenuItem($group, $icon, $text = '')
    {
        $menu = $this->getMainForm()->get('headMenu');
        if ($menu) {
            $btn = new UIButton();
            $btn->align = 'left';
            $btn->w = 27;
            $btn->cursor = 'hand';
            $btn->group = $group;

            if ($icon)
                $btn->setIcon(DN::image($icon));

            $btn->text = $text;
            $menu->add($btn);

            $gap = new UIPanel();
            $gap->align = 'left';
            $gap->w = 2;
            $menu->add($btn);
            return $btn;
        }
    }

    /**
     * @param $group
     * @param $text
     * @param null $icon
     * @return NULL|\php\swing\UIElement|UIMenu
     */
    public function addMenuGroup($group, $text, $icon = null)
    {
        /** @var UIMenuBar $menu */
        $menu = $this->getMainForm()->get('mainMenu');

        /** @var UIMenu $menu */
        $test = $menu->getComponentByGroup($group);
        if (!$test) {
            $item = new UIMenu();
            $item->group = $group;
            $item->text = $text;
            if ($icon)
                $item->setIcon(DN::image($icon));

            $menu->add($item);
            return $item;
        } else
            return $test;
    }

    /**
     * @param $group
     */
    public function addMenuSeparator($group)
    {
        /** @var UIMenuBar $menu */
        $menu = $this->getMainForm()->get('mainMenu');

        /** @var UIMenu $menu */
        $menu = $menu->getComponentByGroup($group);
        if ($menu) {
            $menu->addSeparator();
        }
    }

    /**
     * @param $group
     * @param $id
     * @param $text
     * @param null $icon
     * @param null $accelerator
     * @return null|UIMenuItem
     */
    public function addMenuItem($group, $id, $text, $icon = null, $accelerator = null)
    {
        /** @var UIMenuBar $menu */
        $menu = $this->getMainForm()->get('mainMenu');

        /** @var UIMenu $menu */
        $menu = $menu->getComponentByGroup($group);
        if ($menu) {
            $item = new UIMenuItem();
            $item->text = $text;
            $item->group = $id;
            if ($icon)
                $item->setIcon(DN::image($icon));

            if ($accelerator)
                $item->accelerator = $accelerator;

            $menu->add($item);
            return $item;
        }
        return null;
    }

    public function setMenuHandlers(array $handlers)
    {
        /** @var UIMenuBar $menu */
        $menu = $this->getMainForm()->get('mainMenu');

        /** @var UIPanel $headMenu */
        $headMenu = $this->getMainForm()->get('headMenu');

        foreach ($handlers as $code => $handler) {
            $tmp = str::split($code, ':', 2);
            $sub = $menu->getComponentByGroup($tmp[0]);
            if ($sub instanceof UIMenu) {
                $item = $sub->getComponentByGroup($tmp[1]);
                if ($item) {
                    $item->on('click', $handler);
                }
            }

            $headItem = $headMenu->getComponentByGroup($code);
            if ($headItem) {
                $headItem->on('click', $handler);
            }

            $treePopupItem = $this->_fileTreeMenu->getComponentByGroup($code);
            if ($treePopupItem) {
                $treePopupItem->on('click', $handler);
            }
        }
    }

    public function addFileTreePopupGroup($group, $text)
    {
        $item = new UIMenu();
        $item->text = $text;
        $item->group = $group;

        $this->_fileTreeMenu->add($item);
    }

    public function addFileTreePopupSeparator($group = null)
    {
        /** @var UIMenu $menu */
        $menu = $this->_fileTreeMenu;
        if ($group)
            $menu = $this->_fileTreeMenu->getComponentByGroup($group);

        $menu->addSeparator();
    }

    public function addFileTreePopupItem($group, $id, $text, $icon = null, $accelerator = null)
    {
        $item = new UIMenuItem();
        $item->text = $text;
        $item->group = $id;
        if ($icon)
            $item->setIcon(DN::image($icon));

        if ($accelerator)
            $item->accelerator = $accelerator;

        $menu = $this->_fileTreeMenu;
        if ($group)
            $menu = $this->_fileTreeMenu->getComponentByGroup($group);

        $menu->add($item);
        return $item;
    }

    public function addLocalizationPath($path)
    {
        try {
            DN::i18n()->append(Stream::of($path . '/messages.' . DN::i18n()->getLang()));
        } catch (IOException $e) {

        }
    }

    public function logTool(Tool $tool, File $directory, array $commands, callable $onEnd = null)
    {
        $this->trigger('log-tool', [$tool, $directory, $commands, $onEnd]);
    }

    public function addBackgroundTask(IdeBackgroundTask $task)
    {
        $this->_backgroundTasks[] = $task;
        $task->ideManager = $this;
        $task->execute();
    }

    public function on($event, callable $callback)
    {
        if (is_array($event)) {
            foreach ($event as $el) {
                $this->_handlers[$el][] = $callback;
            }
        } else {
            $this->_handlers[$event][] = $callback;
        }
    }

    public function trigger($event, array $args = [])
    {
        $handlers = (array)$this->_handlers[$event];
        foreach ($handlers as $handler) {
            call_user_func_array($handler, items::toList($this, $args));
        }
    }

    public function cleanUpBackgroundTasks()
    {
        $tasks = [];
        foreach ($this->_backgroundTasks as $task) {
            if (!$task->isDone() && !$task->isCanceled())
                $tasks[] = $task;
        }
        $this->_backgroundTasks = $tasks;
    }

    /**
     * @return \develnext\ide\IdeBackgroundTask[]
     */
    public function getBackgroundTasks()
    {
        return $this->_backgroundTasks;
    }

    /**
     * @return IdeWizard[]
     */
    public function getWizards()
    {
        return $this->_wizards;
    }

    /**
     * @param string $wizardClass
     * @return IdeWizard
     */
    public function getWizard($wizardClass) {
        return $this->_wizards[str::lower(get_class($wizardClass))];
    }

    /**
     * @return ProjectTemplate[]
     */
    public function getProjectTemplates()
    {
        return $this->_projectTemplates;
    }

    /**
     * @return ProjectTemplateCategory[]
     */
    public function getProjectTemplateCategories()
    {
        return $this->_projectTemplateCategories;
    }

    /**
     * @return IDEMainForm
     */
    public function getMainForm()
    {
        /** @var IDEMainForm $form */
        $form = DN::form('MainForm');
        $form->fileTree->popupMenu = $this->_fileTreeMenu;

        return $form;
    }

    /**
     * @param File $file
     * @param Project $project
     * @return FileType|null
     */
    public function getFileTypeOf(File $file, Project $project = null)
    {
        $result = null;

        /** @var FileType $type */
        foreach (DI::findInstanceOf(FileType::class) as $type) {
            if ($type->onDetect($file, $project))
                $result = $type;
        }

        return $result;
    }

    /**
     * @param string $path
     * @param bool $cached
     * @return UXStage
     */
    public function getStage($path, $cached = true)
    {
        if (!str::endsWith($path, '.fxml')) {
            $path .= '.fxml';
        }

        if ($cached && $stage = $this->_cachedForms[str::lower($path)]) {
            return $stage;
        }

        /** @var UXLoader $uxLoader */
        $uxLoader = DI::get(UXLoader::class);
        $uxLoader->location = 'http://develnext.org/';

        return $this->_cachedForms[str::lower($path)] = $uxLoader->loadAsStage(Stream::of('res://' . $path));
    }

    /**
     * @param string $path
     * @param bool $cached
     * @return IDEForm
     */
    public function getForm($path, $cached = true)
    {
        if (!str::endsWith($path, '.xml')) {
            $path .= '.xml';
        }

        if ($cached && $form = $this->_cachedForms[str::lower($path)])
            return $form;

        $uiReader  = DI::get(UIReader::class);
        $localizer = DI::get(Localizer::class);

        $vars = array();

        $uiReader->onRead(function (UIElement $e, $var) use (&$vars) {
            $vars[$var] = $e;
        });

        $uiReader->onTranslate(function (UIElement $e, $text) use ($localizer) {
            return $localizer->translate($text);
        });

        $window = $uiReader->read(Stream::of('res://forms/' . $path));
        try {
            $name = str::split(str::sub($path, 0, str::lastPos($path, '.xml')), '/');
            $module = new Module(Stream::of('res://develnext/forms/' . str::join($name, '/') . '.php'));

            $name[sizeof($name) - 1] = 'IDE' . $name[sizeof($name) - 1];
            $className = 'develnext\\forms\\' . str::join($name, '\\');

            if (class_exists($className, false)) {
                $form = new $className($window, $module, $vars);
            } else {
                $form = new IDEForm($window, $module, $vars);
            }
        } catch (IOException $e) {
            $form = new IDEForm($window, null, $vars);
        }

        $this->_cachedForms[str::lower($path)] = $form;
        return $form;
    }

    /**
     * @param $text
     * @param null|string $icon
     */
    public function setStatusBarText($text, $icon = null)
    {
        /** @var UIPanel $statusBar */
        $statusBar = $this->getMainForm()->get('status-bar');

        /** @var UILabel $status */
        $status = $statusBar->getComponentByGroup('status');

        $status->text = $text;
        $status->setIcon(DN::image($icon));
    }

    public function newElement($xml)
    {
        $st = new MemoryStream();
        $st->write($xml);
        $st->seek(0);


        $uiReader = DI::get(UIReader::class);

        return $uiReader->read($st);
    }
}

