<?php
namespace develnext\ide;

use develnext\DN;
use develnext\FSStream;
use develnext\util\Config;
use develnext\util\XmlConfig;
use ext\xml\DomDocument;
use ext\xml\XmlProcessor;
use php\io\File;
use php\io\FileStream;
use php\lang\System;

class ConfigManager
{
    /** @var File */
    protected $_userDirectory;

    /** @var File */
    protected $_settingsDirectory;

    /** @var XmlConfig[] */
    protected $_xmlCache = [];

    /** @var Config[] */
    protected $_propertiesCache = [];

    public function __construct()
    {
        $this->_userDirectory     = new File(System::getProperty('user.home', './'));
        $this->_settingsDirectory = new File($this->_userDirectory->getPath() . '/.DevelNext_A1');

        if (!$this->_settingsDirectory->exists()) {
            $this->_settingsDirectory->mkdirs();
        }
    }

    /**
     * @return File
     */
    public function userHome()
    {
        return $this->_userDirectory;
    }

    /**
     * @param $name
     * @return File
     */
    public function file($name)
    {
        return new File($this->_settingsDirectory->getPath() . '/' . $name);
    }

    /**
     * @param string $name
     * @return Config
     */
    public function properties($name)
    {
        if ($properties = $this->_propertiesCache[$name]) {
            return $properties;
        }

        $file = $this->file($name . '.conf');
        if (!$file->exists()) {
            $file->createNewFile();
        }

        $properties = new Config($st = new FileStream($file, 'r+'));
        $st->seek(0);

        $this->_propertiesCache[$name] = $properties;

        return $properties;
    }

    /**
     * @param string $name
     * @return XmlConfig
     */
    public function xml($name)
    {
        if ($xml = $this->_xmlCache[$name]) {
            return $xml;
        }

        $file = $this->file($name . '.conf.xml');
        if (!$file->exists()) {
            $file->createNewFile();
        }

        $xml = new XmlConfig(new FileStream($file, 'r+'));

        $this->_xmlCache[$name] = $xml;

        return $xml;
    }

    /**
     * Save all configs.
     */
    public function saveAll()
    {
        DN::log()->debug("Save all configurations");

        foreach ($this->_xmlCache as $xml) {
            $xml->save();
        }

        foreach ($this->_propertiesCache as $properties) {
            $properties->save();
        }
    }
}