<?php
namespace develnext\ide;

use develnext\DN;
use develnext\Bootstrap;
use develnext\project\Project;
use php\lib\str;
use php\swing\UIElement;

abstract class IdeWizard
{
    /** @var Project */
    public $project = null;

    /** @var UIElement[] */
    protected $uiElements = [];

    abstract public function available(Project $project = null);

    abstract public function onOpen($modal = false, array $arguments = []);

    public function create(Project $project = null)
    {
        $result = clone $this;
        $result->project = $project;

        $this->onCreate();
        return $result;
    }

    public function onCreate()
    {
        // nop
    }

    public function onUpdate()
    {
        // nop.
    }

    public function onHide($modalResult = null)
    {
        // nop.
    }

    public function onDelete()
    {
        foreach ($this->uiElements as $element) {
            $element->owner->remove($element);
        }

        $this->uiElements = [];

        return true;
    }

    final public function getUiElements()
    {
        return $this->uiElements;
    }

    final public function delete()
    {
        $wizard = DN::wizard($this);

        if (!$wizard) {
            throw new \LogicException("Wizard '" . get_class($this) . "' is not available");
        }

        return $wizard->onDelete();
    }

    public function hide($modalResult = null)
    {
        $wizard = DN::wizard($this);

        if (!$wizard) {
            throw new \LogicException("Wizard '" . get_class($this) . "' is not available");
        }

        $wizard->onHide($modalResult);
    }

    public function open($modal = false, array $arguments = [])
    {
        $wizard = DN::wizard($this);

        if (!$wizard) {
            throw new \LogicException("Wizard '" . get_class($this) . "' is not available");
        }

        return $wizard->onOpen($modal, $arguments);
    }

    public function openModal(array $arguments = [])
    {
        return $this->open(true, $arguments);
    }

    protected function registerUiElement(UIElement $element)
    {
        $this->uiElements[$element->uid] = $element;
    }

    protected function unregisterUiElement(UIElement $element)
    {
        unset($this->uiElements[$element->uid]);
    }

    protected function addMenuItem($group, $id, $text, $icon = null, $accelerator = null, $action = 'open')
    {
        $item = DN::ide()->addMenuItem($group, $id, $text, $icon, $accelerator);

        $this->registerUiElement($item);

        if ($action) {
            $item->on('click', [$this, $action]);
        }
    }

    protected function addHeadMenuAny(UIElement $element)
    {
        $elements = DN::ide()->addHeadMenuAny($element);

        foreach ($elements as $el) {
            $this->registerUiElement($el);
        }
    }

    protected function addHeadMenuItem($group, $icon = null, $text = '', $action = 'open')
    {
        $button = DN::ide()->addHeadMenuItem($group, $icon, $text);

        $this->registerUiElement($button);

        if ($action) {
            $button->on('click', [$this, $action]);
        }
    }
}