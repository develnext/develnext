<?php
namespace develnext\ide\wizard;

use develnext\DN;
use develnext\ide\components\UIMessages;
use develnext\ide\IdeWizard;
use develnext\project\Project;
use develnext\project\UIProjectChooser;
use develnext\ui\BalloonTip;
use develnext\ui\UIToolTip;
use develnext\validation\ValidationRules;
use php\io\File;
use php\swing\Color;
use php\swing\UIEdit;
use php\swing\UILabel;
use php\util\Regex;

class ProjectLocationWizard extends IdeWizard
{
    public function available(Project $project = null)
    {
        return $project === null;
    }

    public function onOpen($modal = false, array $arguments = [])
    {
        $form = DN::form('ProjectLocationForm');

        /** @var UIEdit $nameEdit */
        $nameEdit     = $form->nameEdit;

        if (isset($arguments['name'])) {
            $nameEdit->text = (string) $arguments['name'];
        }

        $locationBtn  = $form->locationBtn;

        /** @var UIEdit $locationEdit */
        $locationEdit = $form->locationEdit;

        if (isset($arguments['directory'])) {
            $locationEdit->text = $arguments['directory'];
        }

        $nextBtn = $form->nextBtn;

        $locationBtn->on('click', function () use ($locationEdit) {
            $projectChooser = new UIProjectChooser(true);
            $projectChooser->showDialog();

            if ($projectChooser->getSelectedFile()) {
                $locationEdit->text = $projectChooser->getSelectedFile();
            }
        });

        $nextBtn->on('click', function() use ($form, $locationEdit, $nameEdit) {
            if ($form->validate(new ProjectLocationValidationRules())) {
                $form->hide([
                    'directory' => $locationEdit->text,
                    'name' => $nameEdit->text
                ]);
            }
        });

        if ($modal) {
            return $form->showModal();
        } else {
            $form->show();
        }
    }
}

class ProjectLocationValidationRules extends ValidationRules
{
    public $locationEdit = ['path'];
    public $nameEdit     = ['required'];
}