<?php
namespace develnext\ide\wizard;

use develnext\ide\IdeWizard;
use develnext\project\Project;

abstract class ProjectCreationIdeWizard extends IdeWizard
{
    protected $directory;

    /**
     * @param $directory
     */
    public function setProject($directory)
    {
        $this->project = $directory;
    }
}