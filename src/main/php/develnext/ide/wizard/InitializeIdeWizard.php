<?php
namespace develnext\ide\wizard;

use develnext\DN;
use develnext\forms\IDEInitializeForm;
use develnext\ide\IdeWizard;
use develnext\project\Project;
use develnext\ui\decorator\UIListDecorator;
use develnext\util\Config;
use develnext\util\XmlConfig;
use ext\xml\DomNode;
use php\io\ResourceStream;

class InitializeIdeWizard extends IdeWizard
{
    public function available(Project $project = null)
    {
        return $project === null;
    }

    public function onOpen($modal = false)
    {
        /** @var IDEInitializeForm $form */
        $form = DN::form('InitializeForm');
        $list = new UIListDecorator($form->languagesList);

        $allLanguages = [];
        $localizer = DN::i18n();

        $selectedIndex = -1;

        foreach (ResourceStream::getResources('DEVELNEXT-INF/languages.xml') as $resource) {
            $config = new XmlConfig($resource);

            $languages = $config->getDocument()->findAll('/languages/language');

            foreach ($languages as $el) {
                $language = $el->toModel();

                if ($localizer->getLang() === $language['@code']) {
                    $selectedIndex = sizeof($allLanguages);
                    $list->add('<b>' . $language['title'] . '</b>', $language['description'], $language['icon']);
                } else {
                    $list->add($language['title'], $language['description'], $language['icon']);
                }

                $allLanguages[] = $language;
            }
        }

        $form->nextBtn->on('click', function () use ($form, $allLanguages, $list) {
            $config = DN::config()->properties('application');

            if ($list->getElement()->selectedIndex > -1) {
                $config->set('lang', $allLanguages[$list->getElement()->selectedIndex]['@code']);
            }

            $form->hide();
        });

        $list->getElement()->selectedIndex = $selectedIndex;

        $form->showModal();
        return true;
    }
}