<?php

namespace develnext\ide;

use develnext\DN;
use develnext\lang\Singleton;
use php\io\File;
use php\io\IOException;
use php\io\Stream;
use php\lib\str;
use php\swing\Image;

/**
 * Class ImageManager
 * @package develnext\ide
 */
class ImageManager
{
    /** @var Image */
    protected $images;

    /**
     * @param $path
     * @return null|Image
     */
    public function get($path)
    {
        if (str::pos($path, '://') === -1)
            $path = 'res://' . $path;

        if ($r = $this->images[$path])
            return $r;

        try {
            $img = Image::read(Stream::of($path));
            $this->images[$path] = $img;

            DN::log()->debug("Load image '$path'");

            return $img;
        } catch (IOException $e) {
            return null;
        }
    }
}
