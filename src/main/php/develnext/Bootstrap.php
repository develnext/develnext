<?php
namespace develnext;

use develnext\filetype\FileType;
use develnext\forms\IDEMainForm;
use develnext\i18n\Localizer;
use develnext\ide\ConfigManager;
use develnext\ide\IdeManager;
use develnext\ide\IdeWizard;
use develnext\ide\std\StandardIdeExtension;
use develnext\ide\wizard\InitializeIdeWizard;
use develnext\ide\wizard\WelcomeIdeWizard;
use develnext\lang\DI;
use develnext\lang\Singleton;
use develnext\log\ConsoleLogWriter;
use develnext\log\LogManager;
use develnext\log\StreamLogWriter;
use develnext\project\Project;
use develnext\project\ProjectLoader;
use develnext\project\ProjectManager;
use develnext\project\ProjectTemplate;
use develnext\util\Config;
use ext\xml\DomDocument;
use php\io\File;
use php\io\FileStream;
use php\io\IOException;
use php\io\ResourceStream;
use php\io\Stream;
use php\lang\Module;
use php\lang\System;
use php\lib\items;
use php\lib\mirror;
use php\lib\str;
use php\swing\Scope;
use php\swing\SwingUtilities;
use php\swing\Timer;
use php\swing\UIDialog;
use php\swing\UIElement;
use php\swing\UIListbox;
use php\swing\UIManager;
use php\swing\UIProgress;
use php\swing\UIReader;
use php\util\Flow;
use php\util\Scanner;

class Bootstrap
{
    /** @var IDEForm[] */
    protected $forms;

    /** @var File */
    protected $userDirectory;

    /** @var File */
    protected $settingsDirectory;

    /** @var Project */
    public $currentProject;

    /** @var Controller[] */
    public $controllers = [];

    public function __construct()
    {
        UIManager::setLookAndFeel('com.alee.laf.WebLookAndFeel');

        spl_autoload_register(function($className){
            $name = str::replace($className, '\\', '/') . '.php';
            try {
                $module = new Module(Stream::of('res://' . $name));
            } catch (IOException $e) {
                return false;
            }
            $module->call();

            if (class_exists(DN::class, false)) {
                try {
                    DN::log()->debug("Include 'res://{$name}'");
                } catch (\Exception $e) {
                    echo "EXCEPTION: {$e->getMessage()} in {$e->getFile()} on line {$e->getLine()}\n\n";
                    echo $e->getTraceAsString();
                }
            }
        });

        DI::register($logManager = new LogManager());

        DI::register($this);
        DI::register(new UIReader());
        DI::register(new ConfigManager());

        DI::register(new IdeManager());
        DI::register($localizer = new Localizer(System::getProperty("user.language")));

        $logManager->addLogWriter(new ConsoleLogWriter(), LogManager::LEVEL_DEBUG);
        $logManager->addLogWriter(
            new StreamLogWriter(new FileStream(DN::config()->file("application.log"), "w+")),
            LogManager::LEVEL_DEBUG
        );

        try {
            $localizer->append(Stream::of(\ROOT . '/system/languages/initial.messages.' . $localizer->getLang()));
        } catch (IOException $e) {
            // nop.
        }

        $config = DN::config()->properties('application');

        if (!$config->get('init')) {
            $initializeWizard = DN::wizard(InitializeIdeWizard::class);

            if (!$initializeWizard->openModal()) {
                $this->finish();
            }

            $config->set('init', true);
            $config->save();
        }

        // localization
        $localizer->setLang($config->get('lang', 'en'));

        foreach (ResourceStream::getResources('i18n/messages.' . $localizer->getLang()) as $resource) {
            $localizer->append($resource);
        }

        $localizer->append(
            Stream::of(\ROOT . '/system/languages/messages.' . $localizer->getLang())
        );

        DI::register($localizer);
    }

    public function showSplash()
    {
        $form = DN::form('SplashForm');
        $form = $form->getWindow();

        $form->moveToCenter();
        $form->visible = true;

        $timer = new Timer(3000, function () use ($form) {
            $form->position = [-1000, -1000];
        });
        $timer->repeat = false;
        $timer->start();
    }

    protected function loadExtensions()
    {
        $st = null;
        try {
            $list = ResourceStream::getResources('DEVELNEXT-INF/extensions.list');
            foreach ($list as $st) {
                $reader = new Scanner($st);
                while ($reader->hasNextLine()) {
                    $ext = str::trim($reader->nextLine());
                    if ($ext) {
                        $class = new \ReflectionClass($ext);
                        DN::ide()->registerExtension($class->newInstance());
                    }
                }

                $st->close();
            }
        } catch (IOException $e) {
            if ($st)
                $st->close();
            throw $e;
        }
    }

    protected function loadProjects()
    {
        /** @var ProjectManager $projectManager */
        $projectManager = DI::get(ProjectManager::class);
        $projectManager->loadConfiguration();
    }

    public function showWelcome()
    {
        $form = DN::ide()->getMainForm();

        if (!$this->currentProject) {
            /** @var WelcomeIdeWizard $welcome */
            $welcome = DI::get(WelcomeIdeWizard::class);

            if (!$welcome) {
                throw new \LogicException("Unavailable WelcomeIdeWizard class");
            }

            if (!$welcome->openModal()) {
                $this->finish();
            }
        }

        $form->show();
    }

    public function start()
    {
        $this->loadExtensions();
        $this->loadProjects();
        $this->showWelcome();
    }

    public function finish()
    {
        DN::log()->debug('Trigger finish ...');

        DN::config()->saveAll();

        System::halt(0);
    }
}
