<?php
namespace develnext;

use ArrayAccess;
use develnext\lang\DI;
use develnext\ui\BalloonTip;
use develnext\util\Config;
use develnext\validation\ValidationRules;
use develnext\validation\Validator;
use php\io\File;
use php\io\FileStream;
use php\lang\Module;
use php\lib\items;
use php\lib\str;
use php\swing\Color;
use php\swing\Font;
use php\swing\UIAbstractIButton;
use php\swing\UIButton;
use php\swing\UICheckbox;
use php\swing\UICombobox;
use php\swing\UIEdit;
use php\swing\UIEditorArea;
use php\swing\UIElement;
use php\swing\UIForm;
use php\swing\UILabel;
use php\swing\UIListbox;
use php\swing\UIPanel;
use php\swing\UIWindow;
use php\util\Flow;

/**
 * Class IDEForm
 * @package develnext
 */
class IDEForm implements ArrayAccess
{
    /** @var UIWindow */
    protected $window;

    /** @var Module */
    protected $module;

    /** @var UIElement[] */
    protected $vars = [];

    /** @var BalloonTip[] */
    protected $balloonTips = [];

    /** @var mixed */
    public $modalResult;

    function __construct(UIWindow $window, Module $module = null, array $vars = [])
    {
        $this->window = $window;
        $this->vars = $vars;

        if ($module) {
            $this->module = $module;
            $module->call(['form' => $this]);
        }
    }

    /**
     * @return \php\lang\Module
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @return \php\swing\UIWindow
     */
    public function getWindow()
    {
        return $this->window;
    }

    /**
     * @param $name
     * @return UIElement|null
     */
    public function get($name)
    {
        return $this->vars[$name];
    }

    public function show($centered = true)
    {
        if ($centered)
            $this->window->moveToCenter();

        $this->window->visible = true;
        return $this->modalResult;
    }

    public function hide($modalResult = null)
    {
        $this->window->visible = false;
        $this->modalResult = $modalResult;
    }

    public function showModal($centered = true)
    {
        $this->window->modalType = 'APPLICATION_MODAL';
        $this->show($centered);

        return $this->modalResult;
    }

    public function saveToFile(File $file)
    {
        $config = new Config($stream = new FileStream($file, 'w+'));
        $maximized = false;
        if ($this->window instanceof UIForm)
            $maximized = $this->window->maximized;

        if (!$maximized) {
            $config->set('w', $this->window->w);
            $config->set('h', $this->window->h);
            $config->set('x', $this->window->x);
            $config->set('y', $this->window->y);
        }

        if ($this->window instanceof UIForm) {
            $config->set('maximized', $this->window->maximized);
        }

        $config->save();
        $stream->close();
    }

    public function loadFromFile(File $file)
    {
        if ($file->exists()) {
            $config = new Config($stream = new FileStream($file->getPath(), 'r'));

            if ($this->window instanceof UIForm) {
                $this->window->maximized = $config->get('maximized', $this->window->maximized);
            }

            $this->window->w = $config->get('w', $this->window->w);
            $this->window->h = $config->get('h', $this->window->h);
            $this->window->x = $config->get('x', $this->window->x);
            $this->window->y = $config->get('y', $this->window->y);

            $stream->close();
        }
    }

    /**
     * @param $name
     * @return UIElement
     */
    public function __get($name)
    {
        return $this->vars[$name];
    }

    public function offsetExists($offset)
    {
        return !!$this->get($offset);
    }

    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    public function offsetSet($offset, $value)
    {
        throw new \LogicException("Form elements are only for reading");
    }

    public function offsetUnset($offset)
    {
        throw new \LogicException("Form elements are only for reading");
    }

    public function removeAllBalloonTips()
    {
        foreach ($this->balloonTips as $balloonTip) {
            $balloonTip->hide();
            $balloonTip->removeSelf();
        }

        $this->balloonTips = [];
    }

    public function validate(ValidationRules $rules, array $attributes = null)
    {
        $this->removeAllBalloonTips();

        /** @var Validator $validator */
        $validator = DI::get(Validator::class);

        $data = [];

        foreach ($this->vars as $name => $element)
        {
            if ($element instanceof UIEditorArea || $element instanceof UIEdit) {
                $data[$name] = $element->text;
            } else if ($element instanceof UIListbox || $element instanceof UICombobox) {
                $data[$name] = $element->selectedIndex;
            } else if ($element instanceof UIAbstractIButton) {
                $data[$name] = $element->selected;
            }
        }

        if ($attributes) {
            $data = Flow::of($data)
                ->onlyKeys($attributes)
                ->withKeys()
                ->toArray();
        }

        $result = $validator->validate($data, $rules);

        foreach ($result->getErrors() as $name => $messages) {
            $label             = new UILabel();
            $label->foreground = Color::rgb(255, 0, 0);

            $label->cursor     = 'hand';
            $label->text       = str::join($messages, "\n");

            $label->on('click', function () use ($this) {
                $this->removeAllBalloonTips();
            });

            $balloonTip = new BalloonTip($this->get($name), $label, $label->foreground);
            $balloonTip->show();

            $this->balloonTips[] = $balloonTip;
        }

        return !$result->hasErrors();
    }
}
