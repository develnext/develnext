<?php
namespace develnext\lang;

use php\lib\mirror;
use php\lib\str;

class DI {
    /** @var array */
    protected static $singletons = [];

    /** @var array */
    protected static $cache = [];

    /**
     * @param $class
     * @param bool $autoRegister
     * @return null|object
     */
    public static function get($class, $autoRegister = true)
    {
        if ($singleton = self::$singletons[str::lower($class)]) {
            return $singleton;
        }

        if (!$autoRegister) {
            return false;
        }

        $ref = new \ReflectionClass($class);

        $constructor = $ref->getConstructor();
        $params      = $constructor ? $constructor->getParameters() : null;

        if ($params) {
            $args = [];

            foreach ($params as $i => $param) {
                $paramClass = $param->getClass();

                if ($paramClass) {
                    $args[$i] = self::get($paramClass->getName());
                } else {
                    $args[$i] = null;
                }
            }

            $singleton = $constructor->invokeArgs(null, $args);
        } else {
            $singleton = new $class();
        }

        self::register($singleton);
        return $singleton;
    }

    /**
     * @param string $class
     * @return bool
     */
    public function exists($class)
    {
        return isset(self::$singletons[str::lower($class)]);
    }

    /**
     * @param string $class
     * @return array
     */
    public static function findInstanceOf($class) {
        $class = str::lower($class);

        $result = self::$cache[$class];

        if (isset($result)) {
            return $result;
        }

        $result = [];

        foreach (self::$singletons as $singleton) {
            if ($singleton instanceof $class) {
                $result[mirror::typeOf($singleton, true)] = $singleton;
            }
        }

        return self::$cache[$class] = $result;
    }

    /**
     * @param $object
     */
    public static function register($object) {
        $class = mirror::typeOf($object, true);

        self::$singletons[$class] = $object;

        $parent = $object;

        while ($parent = get_parent_class($parent)) {
            if (!self::$singletons[str::lower($parent)]) {
                self::$singletons[str::lower($parent)] = $object;
            }
        }

        self::$cache = [];
    }

    /**
     * @param object $object
     */
    public static function unregister($object) {
        $class = mirror::typeOf($object, true);

        unset(self::$singletons[$class]);

        $parent = $object;
        while ($parent = get_parent_class($parent)) {
            unset(self::$singletons[str::lower($parent)]);
        }

        self::$cache = [];
    }
}