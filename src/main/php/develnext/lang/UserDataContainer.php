<?php
namespace develnext\lang;

use php\lib\items;
use php\util\Flow;

trait UserDataContainer
{
    /** @var array */
    protected $userData = [];

    public function __set($name, $value) {
        $this->userData[$name] = $value;
    }

    public function __get($name) {
        return $this->userData[$name];
    }

    public function __isset($name) {
        return isset($this->userData[$name]);
    }

    public function __unset($name) {
        unset($this->userData[$name]);
    }

    /**
     * @return array
     */
    public function getUserData()
    {
        return $this->userData;
    }

    /**
     * @param mixed $userData
     */
    public function setUserData(array $userData)
    {
        $this->userData = $userData;
    }

    /**
     * @param array $userData
     */
    public function mergeUserData(array $userData)
    {
        $this->userData = Flow::of((array) $this->userData)
            ->append($userData)
            ->withKeys()
            ->toArray();
    }
}