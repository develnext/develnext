<?php
namespace develnext\util;

use develnext\DN;
use ext\xml\DomDocument;
use ext\xml\XmlProcessor;
use php\io\FileStream;
use php\io\Stream;

class XmlConfig
{
    /** @var FileStream */
    protected $stream;

    /** @var XmlProcessor */
    protected $xmlProcessor;

    /** @var DomDocument */
    protected $document;

    /**
     * @param FileStream $stream
     */
    public function __construct(FileStream $stream)
    {
        $this->stream       = $stream;
        $this->xmlProcessor = new XmlProcessor();

        try {
            $this->document = $this->xmlProcessor->parse($stream->readFully());
        } catch (\Exception $e) {
            DN::log()->error($e->getMessage());

            $this->document = $this->xmlProcessor->createDocument();
        }
    }

    public function save()
    {
        DN::log()->debug("Save config '{$this->stream->getPath()}'");

        $this->stream->truncate(0);

        $this->xmlProcessor->formatTo($this->document, $this->stream);

        $this->stream->seek(0);
    }

    public function close()
    {
        $this->stream->close();
    }

    /**
     * @return DomDocument
     */
    public function getDocument()
    {
        return $this->document;
    }
}