package org.develnext.ide.ui;

import net.java.balloontip.BalloonTip;
import org.develnext.ide.ui.classes.WrapBalloonTip;
import org.develnext.jphp.swing.SwingExtension;
import php.runtime.env.CompileScope;

public class UIExtension extends SwingExtension {
    public final static String NAMESPACE = "develnext\\ui\\";

    @Override
    public String getVersion() {
        return "1.0";
    }

    @Override
    public void onRegister(CompileScope scope) {
        registerClass(scope, WrapBalloonTip.class, BalloonTip.class);
    }
}
