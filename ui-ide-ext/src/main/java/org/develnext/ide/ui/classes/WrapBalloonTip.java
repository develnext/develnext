package org.develnext.ide.ui.classes;

import net.java.balloontip.BalloonTip;
import net.java.balloontip.positioners.LeftBelowPositioner;
import net.java.balloontip.styles.ModernBalloonStyle;
import net.java.balloontip.utils.ToolTipUtils;
import org.develnext.ide.ui.UIExtension;
import org.develnext.jphp.swing.classes.WrapColor;
import org.develnext.jphp.swing.classes.components.UIPanel;
import org.develnext.jphp.swing.classes.components.support.UIElement;
import php.runtime.Memory;
import php.runtime.annotation.Reflection;
import php.runtime.annotation.Reflection.Arg;
import php.runtime.annotation.Reflection.Signature;
import php.runtime.env.Environment;
import php.runtime.reflection.ClassEntity;

import java.awt.*;

import static php.runtime.annotation.Reflection.*;

@Name(UIExtension.NAMESPACE + "BalloonTip")
public class WrapBalloonTip extends UIPanel {
    public WrapBalloonTip(Environment env, BalloonTip component) {
        super(env, component);
    }

    public WrapBalloonTip(Environment env, ClassEntity clazz) {
        super(env, clazz);
    }

    @Signature({
            @Arg(value = "component", nativeType = UIElement.class),
            @Arg(value = "contents", nativeType = UIElement.class),
            @Arg(value = "borderColor", optional = @Optional("NULL")),
            @Arg(value = "backgroundColor", optional = @Optional("NULL"))
    })
    public Memory __construct(Environment env, Memory... args) {

        Color borderColor     = args[2].isNull() ? Color.LIGHT_GRAY : WrapColor.of(args[2]);
        Color backgroundColor = args[3].isNull() ? Color.WHITE : WrapColor.of(args[3]);

        component = new BalloonTip(
                args[0].toObject(UIElement.class).getJComponent(),
                args[1].toObject(UIElement.class).getJComponent(),
                new ModernBalloonStyle(3, 3, backgroundColor, backgroundColor, borderColor),
                false
        );

        getComponent().setPositioner(new LeftBelowPositioner(10, 7));
        getComponent().setPadding(6);

        return Memory.NULL;
    }

    @Override
    public BalloonTip getComponent() {
        return (BalloonTip) super.getComponent();
    }

    @Signature
    public Memory show(Environment env, Memory... args) {
        ToolTipUtils.toolTipToBalloon(getComponent());
        return Memory.NULL;
    }
}
