<?php
namespace develnext\ui;

use php\swing\UIElement;
use php\swing\UIPanel;

/**
 * Class BalloonTip
 * @package develnext\ui
 */
class BalloonTip extends UIPanel
{
    /**
     * @param UIElement $component
     * @param UIElement $contents
     * @param $borderColor (optional)
     * @param $backgroundColor (optional)
     */
    public function __construct(UIElement $component, UIElement $contents, $borderColor, $backgroundColor) {}
}